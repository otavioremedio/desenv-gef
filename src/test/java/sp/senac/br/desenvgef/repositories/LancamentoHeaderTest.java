package sp.senac.br.desenvgef.repositories;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import sp.senac.br.desenvgef.entities.LancamentoHeader;
import sp.senac.br.desenvgef.entities.LancamentoLines;

@RunWith(SpringRunner.class)
@SpringBootTest
// @ActiveProfiles("dev")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class LancamentoHeaderTest {

	@Autowired
	private LancamentoHeaderRepository lancamentoHeaderRepository;
	
	@Autowired
	private LancamentoLinesRepository lancamentoLinesRepository;

	static Integer idlancamentoHeader;

	// @After
	// public final void tearDown() {
	// this.tipoDespesaRepository.delete(idTipoDespesa);
	// }

//	@Test
//	public void lancamentoHeaderInsert() throws Exception {
//		LancamentoHeader lancamentoHeader = obterDadosLancamentoHeader();
//		lancamentoHeader = this.lancamentoHeaderRepository.save(lancamentoHeader);
//		idlancamentoHeader = lancamentoHeader.getIdLancamento();
//		assertNotNull(lancamentoHeader.getIdLancamento());
//	}

	 @Test
	 public void testUpdateLancamentoHeader() throws Exception {
		 
//		 LancamentoHeader lancamentoHeader = this.lancamentoHeaderRepository.findOne(64);
//		 List<LancamentoLines> lancamentosLines = new ArrayList<LancamentoLines>();
//		 LancamentoLines linha = new LancamentoLines(lancamentoHeader, null);
//		 linha.setCdUsuIncl("1010");
//		 linha.setDtIncl(new Date());
//		 linha.setCdUsuAlt("123");
//		 lancamentosLines.add(linha);
//		 lancamentoHeader.setLancamentoLines(lancamentosLines);
//		 
//		 lancamentoHeader = this.lancamentoHeaderRepository.save(lancamentoHeader);
		 
		 Integer result = this.lancamentoLinesRepository.getUltimaLinha(64);
		 
	 
	
	 }

//	private LancamentoHeader obterDadosLancamentoHeader() {
//		LancamentoHeader lancamentoHeader = new LancamentoHeader();
//		//LancamentoLines linha = new LancamentoLines(lancamentoHeader);
//		List<LancamentoLines> lancamentosLines = new ArrayList<LancamentoLines>();
//		
//		//linha.setIdLancamento(idLancamento);
////		linha.setNrLinha(1);
////		linha.setCdUsuIncl("1010");
////		linha.setCdUsuAlt("123");
////		
////		lancamentosLines.add(linha);
////		
////		
////		lancamentoHeader.setLancamentoLines(lancamentosLines);
//
//		lancamentoHeader.setIdTipoLancamento(2);
//		lancamentoHeader.setDsLancamento("Teste de Insert via JPA");
//		lancamentoHeader.setCdLancamento("Teste JPA");
//		lancamentoHeader.setDtLancamento(new Date());
//		lancamentoHeader.setDtLancamento(new Date());
//		lancamentoHeader.setIdFormaPagto(2);
//		lancamentoHeader.setIdUnidOper(20);
//		lancamentoHeader.setVlLancamento(100.50);
//		lancamentoHeader.setNmFavorecido("João da Silva");
//		lancamentoHeader.setNrDoctoFavorecido("19");
//		lancamentoHeader.setIdSituacao(1);
//		lancamentoHeader.setVendorId(2);
//		lancamentoHeader.setVendorSiteId(15);
//		lancamentoHeader.setCdUsuIncl("1010");
//		lancamentoHeader.setCdUsuAlt("1033");
//		return lancamentoHeader;
//	}

}
