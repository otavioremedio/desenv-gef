package sp.senac.br.desenvgef.enums;

public enum PerfilEnum {

	ADMIN(272, "ROLE_ADMIN", "Perfil Administrador"),
	PUBLICO(262, "ROLE_PUBLICO", "Perfil Público"),
	APROVADOR(264, "ROLE_APROVADOR", "Perfil Aprovador");
	
	private int cod;
	private String descricao;
	private String descricaoFront;

	private PerfilEnum(int cod, String descricao, String descricaoFront) {
		this.cod = cod;
		this.descricao = descricao;
		this.descricaoFront = descricaoFront;
	}

	public int getCod() {
		return cod;
	}

	public String getDescricao () {
		return descricao;
	}

	public String getDescricaoFront() {
		return descricaoFront;
	}

	public static PerfilEnum toEnum(Integer cod) {

		if (cod == null) {
			return null;
		}

		for (PerfilEnum x : PerfilEnum.values()) {
			if (cod.equals(x.getCod())) {
				return x;
			}
		}

		throw new IllegalArgumentException("Id inválido: " + cod);
	}
}
