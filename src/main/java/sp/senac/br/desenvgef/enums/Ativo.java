package sp.senac.br.desenvgef.enums;

public enum Ativo {

	ATIVO('A', "Ativo"),
	DEMITIDO('D', "Demitido");

	private Character cod;
	private String descricao;

	private Ativo(Character cod, String descricao) {
		this.cod = cod;
		this.descricao = descricao;
	}

	public Character getCod() {
		return cod;
	}

	public String getDescricao() {
		return descricao;
	}

	public static Ativo toEnum(Character cod) {

		if (cod == null) {
			return null;
		}

		for (Ativo x : Ativo.values()) {
			if (cod.equals(x.getCod())) {
				return x;
			}
		}

		throw new IllegalArgumentException("Ativo inválido: " + cod);
	}
}
