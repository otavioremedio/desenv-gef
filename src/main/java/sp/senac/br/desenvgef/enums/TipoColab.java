package sp.senac.br.desenvgef.enums;

public enum TipoColab {

	PRESTADOR('P', "Prestador"),
	FUNCIONARIO('F', "Funcionario"),
	FUNCIONARIO_CLT('A', "Funcionario CLT");
	
	/*
	V	VISITANTE
	C	CANDIDATO
	A	FUNCIONARIO CLT
	E	ESTAGIARIO
	P	PRESTADOR DE SERVICO
	M	APRENDIZ
	U	AUTONOMO
	R	DIRETOR
	T	TEMPORARIO
	H	FUNCIONARIO CLT - INTERMITENTE
	I	FUNCIONARIO CLT - TELETRABALHO
	*/

	private Character cod;
	private String descricao;

	private TipoColab(Character cod, String descricao) {
		this.cod = cod;
		this.descricao = descricao;
	}

	public Character getCod() {
		return cod;
	}

	public String getDescricao() {
		return descricao;
	}

	public static TipoColab toEnum(Character cod) {

		if (cod == null) {
			return null;
		}

		for (TipoColab x : TipoColab.values()) {
			if (cod.equals(x.getCod())) {
				return x;
			}
		}

		throw new IllegalArgumentException("Tipo inválido: " + cod);
	}
}
