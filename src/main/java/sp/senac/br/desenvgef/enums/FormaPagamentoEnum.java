package sp.senac.br.desenvgef.enums;

public enum FormaPagamentoEnum {

	DINHEIRO(1, "Dinheiro"),
	DEPOSITO(2, "Deposito");

	private Integer codigo;
	private String descricao;

	private FormaPagamentoEnum(Integer codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public static FormaPagamentoEnum toEnum(Integer codigo) {
		
		if (codigo == null) {
			return null;
		}

		for (FormaPagamentoEnum tipo : FormaPagamentoEnum.values()) {
			if (codigo.equals(tipo.getCodigo())) {
				return tipo;
			}
		}

		throw new IllegalArgumentException("Tipo inválido: " + codigo);
	}
}
