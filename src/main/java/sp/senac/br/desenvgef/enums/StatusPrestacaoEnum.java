package sp.senac.br.desenvgef.enums;

public enum StatusPrestacaoEnum {

	ABERTO(1, "Aberto"),
	IMPRESSO(2, "Impresso"),
	VALIDADO(3, "Validado");

	private Integer codigo;
	private String descricao;

	private StatusPrestacaoEnum(Integer codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public static StatusPrestacaoEnum toEnum(Integer codigo) {
		
		if (codigo == null) {
			return null;
		}

		for (StatusPrestacaoEnum tipo : StatusPrestacaoEnum.values()) {
			if (codigo.equals(tipo.getCodigo())) {
				return tipo;
			}
		}

		throw new IllegalArgumentException("Status inválido: " + codigo);
	}
}
