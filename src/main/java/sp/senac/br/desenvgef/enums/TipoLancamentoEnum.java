package sp.senac.br.desenvgef.enums;

public enum TipoLancamentoEnum {

	ADIANTAMENTO(1, "Adiantemento"),
	PRESTACAO_CONTA(2, "Prestacão de Contas"),
	REEMBOLSO(3, "ReembolsoDTO");

	private Integer codigo;
	private String descricao;

	private TipoLancamentoEnum(Integer codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public static TipoLancamentoEnum toEnum(Integer codigo) {
		
		if (codigo == null) {
			return null;
		}

		for (TipoLancamentoEnum tipo : TipoLancamentoEnum.values()) {
			if (codigo.equals(tipo.getCodigo())) {
				return tipo;
			}
		}

		throw new IllegalArgumentException("Tipo inválido: " + codigo);
	}
}
