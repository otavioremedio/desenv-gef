package sp.senac.br.desenvgef.enums;

public enum TipoFavorecidoEnum {

	ID(1, "ID"),
	CPF(2, "CPF"),
	CNPJ(3, "CNPJ");

	private Integer codigo;
	private String descricao;

	private TipoFavorecidoEnum(Integer codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public static TipoFavorecidoEnum toEnum(Integer codigo) {
		
		if (codigo == null) {
			return null;
		}

		for (TipoFavorecidoEnum tipo : TipoFavorecidoEnum.values()) {
			if (codigo.equals(tipo.getCodigo())) {
				return tipo;
			}
		}

		throw new IllegalArgumentException("Status inválido: " + codigo);
	}
}
