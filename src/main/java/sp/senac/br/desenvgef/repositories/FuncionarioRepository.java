package sp.senac.br.desenvgef.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import sp.senac.br.desenvgef.entities.Funcionario;

public interface FuncionarioRepository extends JpaRepository<Funcionario, Integer> {
	@Transactional(readOnly = true)
	List<Funcionario> findFirst20ByNomeIgnoreCaseStartsWithAndTipoColab(String paramBusca, Character tipoColab);

	@Transactional(readOnly = true)
	@Query("Select func from Funcionario func where concat(func.cpf, func.digCpf) like concat(:paramBusca, '%') "
					+ " and rownum <= 20")
	List<Funcionario> findFirst20ByCpfStartsWith(@Param("paramBusca") String paramBusca);

	@Query("Select func from Funcionario func where concat(func.cpf, func.digCpf) = :cpf ")
	@Transactional(readOnly = true)
	Funcionario verifyIfExistCpf(@Param("cpf") String cpf);
}
