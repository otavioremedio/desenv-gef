package sp.senac.br.desenvgef.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import sp.senac.br.desenvgef.entities.Favorecido;

public interface FavorecidoRepository extends JpaRepository<Favorecido, Integer> {
	
	@Query("Select fav from Favorecido fav where fav.tipo = '1' and fav.segment1 = :param ")
	@Transactional(readOnly = true)
	Favorecido getFavorecidoPorCPF(@Param("param") String param);
	
	@Query("Select fav from Favorecido fav where fav.tipo = '2' and fav.segment1 = :param ")
	@Transactional(readOnly = true)
	Favorecido getFavorecidoPorCNPJ(@Param("param") String param);
	
	@Query("Select fav from Favorecido fav where fav.tipo = '3' and fav.employeeNumber = :param ")
	@Transactional(readOnly = true)
	Favorecido getFavorecidoPorCodigo(@Param("param") String param);

}
