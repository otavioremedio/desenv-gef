package sp.senac.br.desenvgef.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import sp.senac.br.desenvgef.entities.LancamentoHeader;


public interface LancamentoHeaderRepository extends JpaRepository<LancamentoHeader, Integer> {
	 
	 @Query(nativeQuery = true, value = "SELECT xxcust_ap_prest_cta_int_pk.get_val_extenso_f(:valor) FROM dual ")
	 String getValorExtenso(@Param("valor") double valor);
	 
	 @Query(nativeQuery = true, value = "SELECT xxcust_ap_prest_cta_int_pk.get_num_lancto_f(:tipo) FROM dual ")
	 String getCodigoLancamento(@Param("tipo") int tipo);
	 
	 
}
