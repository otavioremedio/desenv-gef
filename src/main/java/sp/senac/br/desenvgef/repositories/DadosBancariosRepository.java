package sp.senac.br.desenvgef.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import sp.senac.br.desenvgef.entities.DadosBancarios;


public interface DadosBancariosRepository extends JpaRepository<DadosBancarios, Integer> {
	 
	@Query("select banco from DadosBancarios banco where banco.segment1 = :param ")
	@Transactional(readOnly = true)
	List<DadosBancarios> listaDadosBancarios(@Param("param") String param);
	
}
