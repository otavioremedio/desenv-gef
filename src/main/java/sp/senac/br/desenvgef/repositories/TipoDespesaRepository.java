package sp.senac.br.desenvgef.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import sp.senac.br.desenvgef.entities.TipoDespesa;


public interface TipoDespesaRepository extends JpaRepository<TipoDespesa, Integer> {
	 
	@Query("select adt from TipoDespesa adt where adt.tipo = 'XXAP_APP_PREST_CT_ADTO_DESPESA' ")
	@Transactional(readOnly = true)
	List<TipoDespesa> listaTipoAdiantamentos();
	
	@Query("select rmb from TipoDespesa rmb where rmb.tipo = 'XXAP_APPL_PREST_DESPESAS' ")
	@Transactional(readOnly = true)
	List<TipoDespesa> listaTipoReembolso();
	
	@Query("select cta from TipoDespesa cta where cta.tipo = 'XXAP_APP_TP_DESPESA_OUTROS' ")
	@Transactional(readOnly = true)
	List<TipoDespesa> listaContaContabil();
	
	@Query("select cta from TipoDespesa cta where cta.tipo = 'XXAP_APP_TP_ADTO_DIFERENCA' ")
	@Transactional(readOnly = true)
	List<TipoDespesa> listaDiferencaAdiantamento();
	
	
	
}
