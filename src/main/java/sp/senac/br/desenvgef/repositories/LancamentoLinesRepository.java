package sp.senac.br.desenvgef.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import sp.senac.br.desenvgef.entities.LancamentoLines;

public interface LancamentoLinesRepository extends JpaRepository<LancamentoLines, Integer> {

	@Transactional(readOnly = true)
	@Query("select line from LancamentoLines line where id.lancamentoHeader.idLancamento = :codigo")
	List<LancamentoLines> listaLinhasLancamento(@Param("codigo") int codigo);

	@Transactional(readOnly = true)
	@Query("select max(line.id.nrLinha) from LancamentoLines line where id.lancamentoHeader.idLancamento = :codigo")
	Integer getUltimaLinha(@Param("codigo") int codigo);
	
	@Query(nativeQuery = true, value = "SELECT xxcust_ap_prest_cta_int_pk.get_val_despesa_f(:tipo) FROM DUAL ")
	Double getValorKm(@Param("tipo") int tipo);
	
	@Query(nativeQuery = true, value = "SELECT xxcust_ap_prest_cta_int_pk.get_val_centro_custo_f(:chapa, :vendorSite, :orgId, :tipoDespesa, :conta, :centro, :tipoLancamento) FROM DUAL ")
	String validaCentroCusto(
			@Param("chapa") int chapa, 
			@Param("vendorSite") int vendorSite, 
			@Param("orgId") int orgId, 
			@Param("tipoDespesa") int tipoDespesa, 
			@Param("conta") Integer conta, 
			@Param("centro") String centro,
			@Param("tipoLancamento") int tipoLancamento);
}
