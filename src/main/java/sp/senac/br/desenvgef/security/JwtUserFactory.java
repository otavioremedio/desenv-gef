package sp.senac.br.desenvgef.security;

import java.util.ArrayList;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import sp.senac.br.desenvgef.dtos.UsuarioDto;
import sp.senac.br.desenvgef.enums.PerfilEnum;

public class JwtUserFactory {

	private JwtUserFactory() {
	}

	/**
	 * Converte e gera um JwtUser com base nos dados de uma Conta.
	 *
	 * @param conta
	 * @return JwtUser
	 */
	public static JwtUser create(UsuarioDto usuario) {
		return new JwtUser(usuario.getChapa(), usuario.getEmail(), usuario.getPerfil() != null ? mapToGrantedAuthorities(usuario.getPerfil()) : null);
	}

	/**
	 * Converte o perfil do usuário para o formato utilizado pelo Spring Security.
	 *
	 * @param perfilEnum
	 * @return List<GrantedAuthority>
	 */
	private static List<GrantedAuthority> mapToGrantedAuthorities(PerfilEnum perfilEnum) {
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority(perfilEnum.toString()));
		return authorities;
	}

}
