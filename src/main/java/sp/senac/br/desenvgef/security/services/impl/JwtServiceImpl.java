package sp.senac.br.desenvgef.security.services.impl;

import javax.servlet.http.Cookie;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import sp.senac.br.desenvgef.dtos.UsuarioDto;
import sp.senac.br.desenvgef.security.JwtUser;
import sp.senac.br.desenvgef.security.JwtUserFactory;
import sp.senac.br.desenvgef.security.services.JwtService;
import sp.senac.br.desenvgef.security.utils.JwtTokenUtil;

@Service
public class JwtServiceImpl implements JwtService {

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Value("${server.session.cookie.max-age}")
	private Integer cookieDuration;

	@Override
	public String generateTokenJwt(UsuarioDto usuario){

		Authentication authentication = new UsernamePasswordAuthenticationToken(
				usuario.getEmail(), null, usuario.getPerfil() != null ? AuthorityUtils.createAuthorityList(usuario.getPerfil().getDescricao()):null);
		SecurityContextHolder.getContext().setAuthentication(authentication);
		JwtUser jwtUser = JwtUserFactory.create(usuario);
		String token = jwtTokenUtil.obterToken(jwtUser);
		return token;
	}

	@Override
	public Cookie generateCookie(String token){
		Cookie cookie = new Cookie("token", token);
	    cookie.setSecure(false);
	    cookie.setHttpOnly(true);
	    cookie.setMaxAge(cookieDuration);
	    cookie.setPath("/");
	    return cookie;
	}

}
