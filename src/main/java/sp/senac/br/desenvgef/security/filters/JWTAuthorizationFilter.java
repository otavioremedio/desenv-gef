package sp.senac.br.desenvgef.security.filters;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.web.util.WebUtils;

import sp.senac.br.desenvgef.dtos.UsuarioDto;
import sp.senac.br.desenvgef.security.utils.JwtTokenUtil;

//implementação autorizacao
public class JWTAuthorizationFilter extends BasicAuthenticationFilter {

	private JwtTokenUtil jwtTokenUtil;


	public JWTAuthorizationFilter(AuthenticationManager authenticationManager, JwtTokenUtil jwtTokenUtil) {
		super(authenticationManager);
		this.jwtTokenUtil = jwtTokenUtil;
	}

	@Override
    protected void doFilterInternal(HttpServletRequest req,
                                    HttpServletResponse res,
                                    FilterChain chain) throws IOException, ServletException {

		//pegar cabeçalho
		Cookie cookie = WebUtils.getCookie(req, "token");
        String token  = cookie != null ? cookie.getValue() : null;

        HttpSession session = req.getSession(false);
		UsuarioDto usuario = session == null ? null : (UsuarioDto) session.getAttribute("usuario");

		if (token != null && usuario != null && jwtTokenUtil.getUsernameFromToken(token).equals(usuario.getEmail())) {

			UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(usuario, null, AuthorityUtils.createAuthorityList(usuario.getPerfil().getDescricao()));
			if (auth != null) {
				SecurityContextHolder.getContext().setAuthentication(auth);
			}
		}

		chain.doFilter(req, res);
	}

}
