package sp.senac.br.desenvgef.security.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

import sp.senac.br.desenvgef.security.JwtAuthenticationEntryPoint;
import sp.senac.br.desenvgef.security.filters.JWTAuthorizationFilter;
import sp.senac.br.desenvgef.security.utils.JwtTokenUtil;


@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private JwtAuthenticationEntryPoint unauthorizedHandler;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring()
        .antMatchers("/css/**")
		.antMatchers("/js/**")
		.antMatchers("/resources/**")
		.antMatchers("/img/**")
		.antMatchers("/fonts/**");
    }

	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {
		httpSecurity.csrf().disable().exceptionHandling()
		.authenticationEntryPoint(unauthorizedHandler)
		.and()
		.sessionManagement()
		.sessionCreationPolicy(SessionCreationPolicy.NEVER)
		.and()
		.authorizeRequests()
		.antMatchers("/").permitAll()
		.antMatchers("/error").permitAll()
		.antMatchers("/img/**").permitAll()
		.anyRequest()
		.authenticated();
		httpSecurity.addFilter(new JWTAuthorizationFilter(authenticationManager(), jwtTokenUtil));
		httpSecurity.sessionManagement().sessionFixation().migrateSession();
	}

}
