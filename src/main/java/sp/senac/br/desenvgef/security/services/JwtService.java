package sp.senac.br.desenvgef.security.services;

import javax.servlet.http.Cookie;

import sp.senac.br.desenvgef.dtos.UsuarioDto;

public interface JwtService {
	String generateTokenJwt(UsuarioDto usuario);
	Cookie generateCookie(String token);
}
