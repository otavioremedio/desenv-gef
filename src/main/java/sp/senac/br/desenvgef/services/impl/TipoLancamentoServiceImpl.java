package sp.senac.br.desenvgef.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sp.senac.br.desenvgef.entities.TipoDespesa;
import sp.senac.br.desenvgef.repositories.TipoDespesaRepository;
import sp.senac.br.desenvgef.services.TipoLancamentoService;

@Service
public class TipoLancamentoServiceImpl implements TipoLancamentoService {

	@Autowired
	private TipoDespesaRepository tipoDespesaRepository;
	
	@Override
	public List<TipoDespesa> listaTipoAdiantamentos() {
		return this.tipoDespesaRepository.listaTipoAdiantamentos();
	}

	@Override
	public List<TipoDespesa> listaTipoReembolso() {
		return this.tipoDespesaRepository.listaTipoReembolso();
	}

	@Override
	public List<TipoDespesa> listaContaContabil() {
		return this.tipoDespesaRepository.listaContaContabil();
	}

	@Override
	public List<TipoDespesa> listaDiferencaAdiantamento() {
		return this.tipoDespesaRepository.listaDiferencaAdiantamento();
	}
	
	

}
