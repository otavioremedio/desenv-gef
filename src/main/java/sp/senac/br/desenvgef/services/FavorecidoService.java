package sp.senac.br.desenvgef.services;

import sp.senac.br.desenvgef.dtos.SearchDTO;
import sp.senac.br.desenvgef.entities.Favorecido;

public interface FavorecidoService {
	
	Favorecido getFavorecidoPorCPF(String param);
	
	Favorecido getFavorecidoPorCNPJ(String param);
	
	Favorecido getFavorecidoPorCodigo(String param);
	
	Favorecido getFavorecido(SearchDTO searchDTO);

}
