package sp.senac.br.desenvgef.services.impl;

import java.text.MessageFormat;
import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.WebUtils;

import sp.senac.br.desenvgef.dtos.CredenciaisDto;
import sp.senac.br.desenvgef.dtos.MenuDto;
import sp.senac.br.desenvgef.dtos.SistemaParametroDto;
import sp.senac.br.desenvgef.dtos.UsuarioDto;
import sp.senac.br.desenvgef.exceptions.SessionException;
import sp.senac.br.desenvgef.services.MainService;

@Service
@SessionAttributes({"token"})
public class MainServiceImpl implements MainService {

	@Value("${admti.api.menus.url}")
	private String admtiApiMenusUrl;

	@Value("${admti.api.usuario.url}")
	private String admtiApiUsuarioUrl;

	@Value("${admti.api.parametros.url}")
	private String admtiApiParametrosUrl;

	@Value("${admti.api.login.url}")
	private String admtiApiLoginUrl;

	@Value("${admti.api.username}")
	private String usernameApi;

	@Value("${jwt.secret}")
	private String senhaApi;

	@Override
	public void verifySession(HttpServletRequest req) {

		Cookie cookie = WebUtils.getCookie(req, "token");
        if(cookie == null){
			throw new SessionException(null);
		}
	}

	@Override
	public List<MenuDto> listMenus(Integer codSistema, Integer perfil) {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<List<MenuDto>> menu = restTemplate.exchange(MessageFormat.format(admtiApiMenusUrl, codSistema, perfil)
			    , HttpMethod.GET, new HttpEntity<String>(createHeaders(LogInAdmTiService(usernameApi, senhaApi))), new ParameterizedTypeReference<List<MenuDto>>(){});
		return menu.getBody();
	}

	@Override
	public UsuarioDto getUsuario(Integer codSistema, String username) {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<UsuarioDto> usuario = restTemplate.exchange(MessageFormat.format(admtiApiUsuarioUrl, codSistema, username)
			    , HttpMethod.GET, new HttpEntity<String>(createHeaders(LogInAdmTiService(usernameApi, senhaApi))) , new ParameterizedTypeReference<UsuarioDto>(){});
		usuario.getBody().setCodSistema(codSistema);
		return usuario.getBody();
	}

	@Override
	public SistemaParametroDto getParametro(Integer codSistema, String nomeParametro) {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<SistemaParametroDto> parametro = restTemplate.exchange(MessageFormat.format(admtiApiParametrosUrl, codSistema.toString(), nomeParametro)
			    , HttpMethod.GET, new HttpEntity<String>(createHeaders(LogInAdmTiService(usernameApi, senhaApi))) , new ParameterizedTypeReference<SistemaParametroDto>(){});
		return parametro.getBody();
	}

	private String LogInAdmTiService(String username, String senha){
		CredenciaisDto credenciais = new CredenciaisDto();
		credenciais.setUsername(username);
		credenciais.setSenha(senha);
		HttpEntity<?> entity = new HttpEntity<>(credenciais, null);
		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<String> response = restTemplate.exchange(admtiApiLoginUrl , HttpMethod.POST, entity, String.class);
		HttpHeaders headers = response.getHeaders();
		String token = headers.get("Authorization").stream().filter(x -> x.toString().startsWith("Bearer")).findFirst().get();
		return token;
	}

	private HttpHeaders createHeaders(String token) {
		HttpHeaders hh = new HttpHeaders();
		hh.set("Authorization", token);
		return hh;
	}

}
