package sp.senac.br.desenvgef.services;

import java.util.List;

import sp.senac.br.desenvgef.entities.LancamentoLines;

public interface LancamentoLinesService {

	List<LancamentoLines> listaLinhasLancamento(int codigo);

	Integer getUltimaLinha(int codigo);
	
	Double getValorKm(int tipo);
	
	String validaCentroCusto(int chapa, int vendorSite, int orgId, int tipoDespesa, Integer conta, String centro, int tipoLancamento);
	
}
