package sp.senac.br.desenvgef.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sp.senac.br.desenvgef.dtos.SearchDTO;
import sp.senac.br.desenvgef.entities.Favorecido;
import sp.senac.br.desenvgef.repositories.FavorecidoRepository;
import sp.senac.br.desenvgef.services.FavorecidoService;


@Service
public class FavorecidoServiceImpl implements FavorecidoService {

	@Autowired
	private FavorecidoRepository favorecidoRepository;

	public Favorecido getFavorecidoPorCPF(String param) {
		return this.favorecidoRepository.getFavorecidoPorCPF(param);
	}

	public Favorecido getFavorecidoPorCNPJ(String param) {
		return this.favorecidoRepository.getFavorecidoPorCNPJ(param);
	}

	public Favorecido getFavorecidoPorCodigo(String param) {
		return this.favorecidoRepository.getFavorecidoPorCodigo(param);
	}
	
	public Favorecido getFavorecido(SearchDTO searchDTO) {
		Favorecido favorecido = new Favorecido(); 
    	switch (searchDTO.getTipo()) {
        case "ID":
        	favorecido = getFavorecidoPorCodigo(searchDTO.getParametro());
            break;
        case "CPF":
        	String cpfMaskParam = searchDTO.getParametro().replaceAll("\\.", "").replaceAll("\\-", "");
        	favorecido = getFavorecidoPorCPF(cpfMaskParam);        	
            break;
        case "CNPJ": 
        	String cnpjMaskParam = searchDTO.getParametro().replaceAll("\\.", "").replaceAll("\\-", "").replaceAll("\\/", "");
        	favorecido = getFavorecidoPorCNPJ(cnpjMaskParam);  
            break;
    	}
		return favorecido;
	}
}
