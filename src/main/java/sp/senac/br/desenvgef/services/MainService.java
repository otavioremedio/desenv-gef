package sp.senac.br.desenvgef.services;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import sp.senac.br.desenvgef.dtos.MenuDto;
import sp.senac.br.desenvgef.dtos.SistemaParametroDto;
import sp.senac.br.desenvgef.dtos.UsuarioDto;

public interface MainService {

	void verifySession(HttpServletRequest req);
	List<MenuDto> listMenus(Integer sistema, Integer perfil);
	SistemaParametroDto getParametro(Integer codSistema, String nomeParametro);
	UsuarioDto getUsuario(Integer codSistema, String username);
}
