package sp.senac.br.desenvgef.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sp.senac.br.desenvgef.entities.LancamentoLines;
import sp.senac.br.desenvgef.repositories.LancamentoLinesRepository;
import sp.senac.br.desenvgef.services.LancamentoLinesService;

@Service
public class LancamentoLinesServiceImpl implements LancamentoLinesService {

	@Autowired
	private LancamentoLinesRepository lancamentoLinesRepository;
	
	@Override
	public List<LancamentoLines> listaLinhasLancamento(int codigo) {
		return this.lancamentoLinesRepository.listaLinhasLancamento(codigo);
	}

	@Override
	public Integer getUltimaLinha(int codigo) {
		return this.lancamentoLinesRepository.getUltimaLinha(codigo);
	}

	@Override
	public Double getValorKm(int tipo) {
		return this.lancamentoLinesRepository.getValorKm(tipo);
	}

	@Override
	public String validaCentroCusto(int chapa, int vendorSite, int orgId, int tipoDespesa, Integer conta, String centro,
			int tipoLancamento) {
		return this.lancamentoLinesRepository.validaCentroCusto(chapa, vendorSite, orgId, tipoDespesa, conta, centro, tipoLancamento);
	}
	
}
