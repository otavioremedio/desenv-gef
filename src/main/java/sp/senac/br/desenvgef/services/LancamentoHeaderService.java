package sp.senac.br.desenvgef.services;

import sp.senac.br.desenvgef.entities.LancamentoHeader;

public interface LancamentoHeaderService {

	LancamentoHeader findOne(Integer id);
	LancamentoHeader save(LancamentoHeader lancamento);	
	String getExtenso(double valor);
	String getCodigoLancamento(int tipo);
	
}
