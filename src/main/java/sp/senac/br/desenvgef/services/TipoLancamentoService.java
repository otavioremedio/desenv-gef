package sp.senac.br.desenvgef.services;

import java.util.List;

import sp.senac.br.desenvgef.entities.TipoDespesa;

public interface TipoLancamentoService {
	
	List<TipoDespesa> listaTipoAdiantamentos();
	
	List<TipoDespesa> listaTipoReembolso();
	
	List<TipoDespesa> listaContaContabil();
	
	List<TipoDespesa> listaDiferencaAdiantamento();

}
