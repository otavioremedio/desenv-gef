package sp.senac.br.desenvgef.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sp.senac.br.desenvgef.entities.Funcionario;
import sp.senac.br.desenvgef.repositories.FuncionarioRepository;
import sp.senac.br.desenvgef.services.FuncionarioService;

@Service
public class FuncionarioServiceImpl implements FuncionarioService {

	@Autowired
	private FuncionarioRepository funcionarioRepository;

	@Override
	public Funcionario findOne(Integer id) {
		return this.funcionarioRepository.findOne(id);
	}

	@Override
	public boolean verifyIfExistCpf(String cpf) {

		Funcionario funcionario = this.funcionarioRepository.verifyIfExistCpf(cpf);
		return (funcionario != null);
	}

	@Override
	@Transactional
	public Funcionario save(Funcionario funcionario) {
		this.funcionarioRepository.save(funcionario);
		return funcionario;
	}

}
