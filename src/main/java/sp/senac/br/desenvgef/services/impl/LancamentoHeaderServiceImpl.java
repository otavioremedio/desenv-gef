package sp.senac.br.desenvgef.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sp.senac.br.desenvgef.entities.LancamentoHeader;
import sp.senac.br.desenvgef.repositories.LancamentoHeaderRepository;
import sp.senac.br.desenvgef.services.LancamentoHeaderService;

@Service
public class LancamentoHeaderServiceImpl implements LancamentoHeaderService {

	@Autowired
	private LancamentoHeaderRepository lancamentoRepository;
	

	public LancamentoHeader findOne(Integer id) {
		return this.lancamentoRepository.findOne(id);
	}
	
	@Transactional
	public LancamentoHeader save(LancamentoHeader lancamento) {
		this.lancamentoRepository.save(lancamento);
		return lancamento;
	}

	@Override
	public String getExtenso(double valor) {
		return this.lancamentoRepository.getValorExtenso(valor);
	}

	@Override
	public String getCodigoLancamento(int tipo) {
		return this.lancamentoRepository.getCodigoLancamento(tipo);
	}	
	
}
