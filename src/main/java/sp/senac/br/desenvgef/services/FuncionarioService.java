package sp.senac.br.desenvgef.services;

import sp.senac.br.desenvgef.entities.Funcionario;

public interface FuncionarioService {

	Funcionario findOne(Integer id);
	boolean verifyIfExistCpf(String cpf);
	Funcionario save(Funcionario funcionario);
}
