package sp.senac.br.desenvgef.services;

import java.util.List;

import sp.senac.br.desenvgef.entities.DadosBancarios;

public interface DadosBancariosService {
	
	List<DadosBancarios> listaDadosBancarios(String param);

}
