package sp.senac.br.desenvgef.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sp.senac.br.desenvgef.entities.DadosBancarios;
import sp.senac.br.desenvgef.repositories.DadosBancariosRepository;
import sp.senac.br.desenvgef.services.DadosBancariosService;

@Service
public class DadosBancariosServiceImpl implements DadosBancariosService {

	@Autowired
	private DadosBancariosRepository dadosBancariosRepository;

	@Override
	public List<DadosBancarios> listaDadosBancarios(String param) {
		return this.dadosBancariosRepository.listaDadosBancarios(param);
	}

}
