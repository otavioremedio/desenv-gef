package sp.senac.br.desenvgef.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import sp.senac.br.desenvgef.dtos.PrestacaoContaDTO;
import sp.senac.br.desenvgef.dtos.UsuarioDto;
import sp.senac.br.desenvgef.entities.DadosBancarios;
import sp.senac.br.desenvgef.entities.Favorecido;
import sp.senac.br.desenvgef.entities.LancamentoHeader;
import sp.senac.br.desenvgef.entities.LancamentoLines;
import sp.senac.br.desenvgef.entities.TipoDespesa;
import sp.senac.br.desenvgef.services.LancamentoHeaderService;
import sp.senac.br.desenvgef.services.LancamentoLinesService;
import sp.senac.br.desenvgef.services.TipoLancamentoService;


@Controller("/reembolso/prestacao")
@SessionAttributes({"usuario","menus","favorecido", "dadosBancarios"})
public class PrestacaoContaController {
	
	@Autowired
	private TipoLancamentoService tipoLancamentoService;
	
	@Autowired
	private LancamentoHeaderService lancamentoHeaderService;
	
	@Autowired
	private LancamentoLinesService lancamentoLinesService;
	
	List<LancamentoLines> linhadespesas = new ArrayList<LancamentoLines>();
	
	
	@RequestMapping(value="/prestacao-form")
    public ModelAndView prestacaoContaPage(@ModelAttribute("prestacao") PrestacaoContaDTO prestacaoContaDTO, HttpServletRequest request, Model model) {
    	
    	ModelAndView mav = new ModelAndView("prestacao-page", "prestacao-entity", new PrestacaoContaDTO());
    	List<TipoDespesa> tipos = this.tipoLancamentoService.listaTipoReembolso();
    	List<TipoDespesa> contas = this.tipoLancamentoService.listaContaContabil();
        
    	prestacaoContaDTO.setTiposDeReembolso(tipos); 
    	prestacaoContaDTO.setContascontabeis(contas);
        model.addAttribute("prestacao", prestacaoContaDTO);
        mav.addObject("tiposMap", tipos);
        mav.addObject("contasMap", contas);
        return mav;
         
    }
	 
	@GetMapping(value="/prestacao-form/{id}")
	public ModelAndView find(@ModelAttribute("prestacao") PrestacaoContaDTO prestacaoContaDTO, @PathVariable Integer id, HttpServletResponse resp, ModelMap model){
		
		LancamentoHeader lancamento = this.lancamentoHeaderService.findOne(id); 
		linhadespesas = this.lancamentoLinesService.listaLinhasLancamento(id);
		
		Map<String, String> dados = new HashMap<String, String>();
		if(prestacaoContaDTO.getFormaPagamento().equals("Deposito")) {
			List<DadosBancarios> lst = prestacaoContaDTO.getDadosBancarios();
	    	for(DadosBancarios banco : lst){
	    		String descricao = String.format("Banco: %s, Agência: %s | Conta: %s", banco.getBanco(), banco.getAgencia(), banco.getConta());
	    	    dados.put(banco.getBankAccountId(), descricao);
	    	}	
		}		
		
		List<TipoDespesa> tipos = this.tipoLancamentoService.listaTipoReembolso();
		List<TipoDespesa> contas = this.tipoLancamentoService.listaContaContabil();
		prestacaoContaDTO.setIdLancamento(id);
		prestacaoContaDTO.setCodigoLancamento(lancamento.getCdLancamento());
		
		prestacaoContaDTO.setCentrocusto("");
		prestacaoContaDTO.setTipoReembolso("");
		prestacaoContaDTO.setNrdocumento("");
		prestacaoContaDTO.setQuantidade("");
		prestacaoContaDTO.setValorDespesa("");
		
		ModelAndView mav = new ModelAndView("prestacao-page", "prestacao-entity", prestacaoContaDTO);
		model.addAttribute("prestacao", prestacaoContaDTO);
		mav.addObject("tiposMap", tipos);
		mav.addObject("contasMap", contas);
		mav.addObject("lists", linhadespesas);		
		if(prestacaoContaDTO.getFormaPagamento().equals("Deposito"))
			mav.addObject("mapbank", dados);
		
		return mav;

	}
    
    @SuppressWarnings("unchecked")
	@PostMapping(value="/process-prestacao")
	public String save(@ModelAttribute PrestacaoContaDTO prestacaoContaDTO, HttpServletRequest request, RedirectAttributes redirect){
    	
    	HttpSession session = request.getSession(false);
    	UsuarioDto usuario = null;
    	Favorecido favorecido = null;
    	List<DadosBancarios> dadosBancarios = null;
    	if (session != null) {
    		usuario = (UsuarioDto) session.getAttribute("usuario");
    		favorecido = (Favorecido) session.getAttribute("favorecido");
    		dadosBancarios = (List<DadosBancarios>) session.getAttribute("dadosBancarios");
    	}
    	
    	LancamentoHeader lancamentoHeader = new LancamentoHeader();
		List<LancamentoLines> lancamentosLines = new ArrayList<LancamentoLines>();
    	
    	String valorNoMask = prestacaoContaDTO.getValorDespesa().replaceAll("\\.", "").replaceAll("\\,", ".");
		double valor = Double.parseDouble(valorNoMask);
		String codigo = this.lancamentoHeaderService.getCodigoLancamento(3);
    	
		if (prestacaoContaDTO.getIdLancamento() != null && prestacaoContaDTO.getIdLancamento() != 0) {
    		
    		int ultimaLinha = 0;
    		int proximaLinha = 0;
    		
    		lancamentoHeader = this.lancamentoHeaderService.findOne(prestacaoContaDTO.getIdLancamento());
    		lancamentosLines = this.lancamentoLinesService.listaLinhasLancamento(prestacaoContaDTO.getIdLancamento());
    		
    		if(lancamentoHeader!= null) {
    			
    			ultimaLinha = lancamentoLinesService.getUltimaLinha(prestacaoContaDTO.getIdLancamento());
    			proximaLinha = ultimaLinha + 1;
    			
    			LancamentoLines linha = new LancamentoLines(lancamentoHeader, proximaLinha);
        		linha.setIdDespesa(Integer.valueOf(prestacaoContaDTO.getTipoReembolso()));
        		linha.setDtInicial(new Date());
        		linha.setDsDespesa(prestacaoContaDTO.getHistorico());
        		linha.setVlDespesa(valor);
        		linha.setCdCentroCusto(prestacaoContaDTO.getCentrocusto());
        		if(prestacaoContaDTO.getQuantidade() != null && !prestacaoContaDTO.getQuantidade().isEmpty())
        			linha.setQtDespesa(Integer.valueOf(prestacaoContaDTO.getQuantidade()));    		
        		linha.setNrDoctoDespesa(prestacaoContaDTO.getNrdocumento());
        		linha.setDsDespesa(prestacaoContaDTO.getHistorico());
        		linha.setCdUsuIncl(String.valueOf(usuario.getChapa()));
        		linha.setCdUsuAlt(String.valueOf(usuario.getChapa()));
        		linha.setDtIncl(new Date());
        		linha.setDtAlt(new Date());
        		
        		lancamentosLines.add(linha);
        		lancamentoHeader.setLancamentoLines(lancamentosLines);
        		
        		lancamentoHeader.setIdTipoLancamento(3);
        		lancamentoHeader.setDsLancamento(prestacaoContaDTO.getDetalhamento());
        		lancamentoHeader.setCdLancamento(codigo); 
        		lancamentoHeader.setDtLancamento(new Date());
        		
        		
        		if(prestacaoContaDTO.getFormaPagamento().equals("Dinheiro"))
        			lancamentoHeader.setIdFormaPagto(1);
        		else
        			lancamentoHeader.setIdFormaPagto(2);
        		
        		lancamentoHeader.setIdUnidOper(favorecido.getOrgId());

        		lancamentoHeader.setVlLancamento(valor);
        		lancamentoHeader.setNmFavorecido(favorecido.getVendorName()); 
        		lancamentoHeader.setNrDoctoFavorecido(favorecido.getSegment1()); 
        		lancamentoHeader.setIdSituacao(1); 
        		lancamentoHeader.setVendorId(favorecido.getVendorId()); 
        		lancamentoHeader.setVendorSiteId(favorecido.getVendorSiteId()); 
        		lancamentoHeader.setCdUsuIncl(String.valueOf(usuario.getChapa()));
        		lancamentoHeader.setCdUsuAlt(null);
        		
        		if(prestacaoContaDTO.getContacorrente() != null && !prestacaoContaDTO.getContacorrente().isEmpty()) {
        			lancamentoHeader.setBankAccountId(Integer.valueOf(prestacaoContaDTO.getContacorrente()));
        		}else {
        			lancamentoHeader.setBankAccountId(null);
        		}
        		
        		
        		lancamentoHeader = this.lancamentoHeaderService.save(lancamentoHeader);
    		}	
    		
    	}else {
    		
    		LancamentoLines linha = new LancamentoLines(lancamentoHeader, 1);
    		linha.setIdDespesa(Integer.valueOf(prestacaoContaDTO.getTipoReembolso()));
    		linha.setDtInicial(new Date());
    		linha.setDsDespesa(prestacaoContaDTO.getHistorico());
    		linha.setVlDespesa(valor);
    		linha.setCdCentroCusto(prestacaoContaDTO.getCentrocusto());
    		if(prestacaoContaDTO.getQuantidade() != null && !prestacaoContaDTO.getQuantidade().isEmpty())
    			linha.setQtDespesa(Integer.valueOf(prestacaoContaDTO.getQuantidade()));    		
    		linha.setNrDoctoDespesa(prestacaoContaDTO.getNrdocumento());
    		linha.setDsDespesa(prestacaoContaDTO.getHistorico());
    		linha.setCdUsuIncl(String.valueOf(usuario.getChapa()));
    		linha.setCdUsuAlt(null);		
    		lancamentosLines.add(linha);
    		lancamentoHeader.setLancamentoLines(lancamentosLines);    		
    		lancamentoHeader.setIdTipoLancamento(3);
    		lancamentoHeader.setDsLancamento(prestacaoContaDTO.getDetalhamento());
    		lancamentoHeader.setCdLancamento(codigo); 
    		lancamentoHeader.setDtLancamento(new Date());    		
    		if(prestacaoContaDTO.getFormaPagamento().equals("Dinheiro"))
    			lancamentoHeader.setIdFormaPagto(1);
    		else
    			lancamentoHeader.setIdFormaPagto(2);
    		
    		lancamentoHeader.setIdUnidOper(favorecido.getOrgId());    		
    		lancamentoHeader.setVlLancamento(valor);
    		lancamentoHeader.setNmFavorecido(favorecido.getVendorName()); 
    		lancamentoHeader.setNrDoctoFavorecido(favorecido.getSegment1()); 
    		lancamentoHeader.setIdSituacao(1); 
    		lancamentoHeader.setVendorId(favorecido.getVendorId()); 
    		lancamentoHeader.setVendorSiteId(favorecido.getVendorSiteId()); 
    		lancamentoHeader.setCdUsuIncl(String.valueOf(usuario.getChapa()));
    		lancamentoHeader.setCdUsuAlt(null);
    		
    		if(prestacaoContaDTO.getContacorrente() != null && !prestacaoContaDTO.getContacorrente().isEmpty()) {
    			lancamentoHeader.setBankAccountId(Integer.valueOf(prestacaoContaDTO.getContacorrente()));
    		}else {
    			lancamentoHeader.setBankAccountId(null);
    		}
    		
    		lancamentoHeader = this.lancamentoHeaderService.save(lancamentoHeader);
    		
    	}
    	
		prestacaoContaDTO.setDadosBancarios(dadosBancarios);
    	redirect.addFlashAttribute("prestacao", prestacaoContaDTO);    	
    	return "redirect:/prestacao-form/" + lancamentoHeader.getIdLancamento();
	}
    
    /*
    @RequestMapping(value="/validaValorKM", method = RequestMethod.GET )
    public @ResponseBody Double buscarValorExtenso(@RequestParam(value = "tipo") Integer tipo, HttpServletRequest request) {  
    	Double valorKM = this.lancamentoLinesService.getValorKm(tipo);
    	return valorKM;
     }
    
    @RequestMapping(value="/validaCentroCusto", method = RequestMethod.GET )
    public @ResponseBody StringResponseDTO validaCentroCusto(@RequestParam(value = "tipo") Integer tipo, 
    		@RequestParam(value = "conta", required=false) Integer conta, @RequestParam(value = "centro") String centro, HttpServletRequest request) {  
    	
    	HttpSession session = request.getSession(false);
    	UsuarioDto usuario = null;
    	Favorecido favorecido = null;
    	if (session != null) {
    		usuario = (UsuarioDto) session.getAttribute("usuario");
    		favorecido = (Favorecido) session.getAttribute("favorecido");
    	}
    	
    	String retornofn = this.lancamentoLinesService.validaCentroCusto(usuario.getChapa(), favorecido.getVendorSiteId(), favorecido.getOrgId(), tipo, null, centro, 3);
    	StringResponseDTO response = new StringResponseDTO(retornofn);
    	
    	return response;
     }*/
      
}
