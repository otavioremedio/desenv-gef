package sp.senac.br.desenvgef.controllers;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/homologacao")
@Profile("dev")
public class HomologacaoController {

	@Autowired
	private MainController mainController;

	@GetMapping(value="/{idPerfil}")
	@Profile("dev")
	public String indexHomolog(HttpServletRequest req, HttpServletResponse res, ModelMap model, @PathVariable Integer idPerfil, RedirectAttributes redirectAttributes) throws IOException{
		Optional<Integer> id = Optional.of(idPerfil);
		return this.mainController.index(req, res, model, id, redirectAttributes);
	}
}

