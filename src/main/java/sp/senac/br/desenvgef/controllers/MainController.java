package sp.senac.br.desenvgef.controllers;

import static sp.senac.br.desenvgef.enums.TipoFavorecidoEnum.ID;
import static sp.senac.br.desenvgef.enums.TipoLancamentoEnum.REEMBOLSO;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.ResourceBundle;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.util.WebUtils;

import sp.senac.br.desenvgef.config.Messages;
import sp.senac.br.desenvgef.dtos.MenuDto;
import sp.senac.br.desenvgef.dtos.SearchDTO;
import sp.senac.br.desenvgef.dtos.StringResponseDTO;
import sp.senac.br.desenvgef.dtos.UsuarioDto;
import sp.senac.br.desenvgef.entities.DadosBancarios;
import sp.senac.br.desenvgef.entities.Favorecido;
import sp.senac.br.desenvgef.exceptions.SystemAccessDenied;
import sp.senac.br.desenvgef.security.services.JwtService;
import sp.senac.br.desenvgef.security.utils.JwtTokenUtil;
import sp.senac.br.desenvgef.services.DadosBancariosService;
import sp.senac.br.desenvgef.services.FavorecidoService;
import sp.senac.br.desenvgef.services.LancamentoHeaderService;
import sp.senac.br.desenvgef.services.LancamentoLinesService;
import sp.senac.br.desenvgef.services.MainService;

@Controller
@RequestMapping("/")
@SessionAttributes({"menus","usuario"})
public class MainController {

	@Autowired
	private LancamentoHeaderService lancamentoHeaderService;
	
	@Autowired
	private DadosBancariosService dadosBancariosService;
	
	@Autowired
	private FavorecidoService favorecidoService;
	
	@Autowired
	private LancamentoLinesService lancamentoLinesService;
	
	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private JwtService jwtService;

	@Autowired
	private MainService mainService;

	@Autowired
	private Messages messages;

	@Value("${environment}")
	private String ambiente;

	@Value("${usuario.desenv}")
	private String usuarioDesenv;

	@Value("${cod.sistema}")
	private Integer codSistema;

	@Value("${admti.path}")
	private String admtiPath;

	private final static String DESENVOLVIMENTO = "desenvolvimento";
	private final static String HOMOLOGACAO = "homologacao";


	@GetMapping
	public String index(HttpServletRequest req, HttpServletResponse res, ModelMap model, Optional<Integer> idPerfil, RedirectAttributes redirectAttributes) throws IOException{
		String login = null;
		HttpSession session = req.getSession(false);
		UsuarioDto usuario = null;
		List<MenuDto> menus = null;
		Cookie cookie = null;
		String tokenAdmTi = null;
		String token = null;

		//usuario do serviço


		if (session != null) {
			usuario = (UsuarioDto) session.getAttribute("usuario");
			menus = (List<MenuDto>) session.getAttribute("menus");
			cookie = WebUtils.getCookie(req, "token");

			if(idPerfil.isPresent()){
				session.removeAttribute("usuario");
				session.removeAttribute("menus"); 
			}
		}

		if (usuario == null || menus == null || cookie == null || idPerfil.isPresent()) {

			login = getLogin(req, login);
			usuario = this.mainService.getUsuario(codSistema, login);

			try {
				//gera token da aplicação
				token = this.jwtService.generateTokenJwt(usuario);
				res.addCookie(this.jwtService.generateCookie(token));
			} catch (Exception e) {
				throw new SystemAccessDenied(e.getMessage());
			}

			//troca o usuario para homologacao
			if (idPerfil.isPresent()) {
				usuario.getSistemasPerfis().get(0).setCodSistemaPerfil(idPerfil.get());
			}

			menus = this.mainService.listMenus(codSistema, usuario.getPerfil().getCod());
			model.addAttribute("usuario",usuario);
			model.addAttribute("menus",menus);
			redirectAttributes.addFlashAttribute("usuario", usuario);
			redirectAttributes.addFlashAttribute("menus", menus);
			
		}
		
////		//lucao daqui pra baixo:
//		List<String> list = new ArrayList<String>();
//		list.add("List A");
//		list.add("List B");
//		list.add("List C");
//		list.add("List D");
//
//		//return back to index.jsp
//		ModelAndView mv = new ModelAndView("home");
//		mv.addObject("lists", list);
//		return mv;

		return "home";
	}


	@GetMapping(value="/session")
	public ResponseEntity<String> verifySession(HttpServletRequest req){
		mainService.verifySession(req);
		return ResponseEntity.status(HttpStatus.OK).body("ok");
	}

	@GetMapping(value="/js/messages.js")
	public String messages(ModelMap model, HttpServletRequest req){
		Locale locale = RequestContextUtils.getLocale(req);
		ResourceBundle msgs = ResourceBundle.getBundle("messages", locale);
		model.addAttribute("msgs", msgs.getKeys());
		return "messages";
	}

	@GetMapping(value="/perfil")
	@PreAuthorize("hasAnyRole('ADMIN')")
	public String perfil(ModelMap model){
		model.addAttribute("idSistema", codSistema);
		model.addAttribute("admtiPath", admtiPath);
		return "infoges/perfil";
	}

	@GetMapping(value="/sistema_perfil")
	@PreAuthorize("hasAnyRole('ADMIN')")
	public String sistemaPerfil(ModelMap model){
		model.addAttribute("idSistema", codSistema);
		model.addAttribute("admtiPath", admtiPath);
		return "infoges/sistema_perfil";
	}

	@GetMapping(value="/menu")
	@PreAuthorize("hasAnyRole('ADMIN')")
	public String menu(ModelMap model){
		model.addAttribute("idSistema", codSistema);
		model.addAttribute("admtiPath", admtiPath);
		return "infoges/menu";
	}

	@GetMapping(value="/parametros")
	@PreAuthorize("hasAnyRole('ADMIN')")
	public String parametros(ModelMap model){
		model.addAttribute("idSistema", codSistema);
		model.addAttribute("admtiPath", admtiPath);
		return "infoges/parametros";
	}

	@GetMapping(value="/mensagem")
	@PreAuthorize("hasAnyRole('ADMIN')")
	public String mensagem(ModelMap model){
		model.addAttribute("idSistema", codSistema);
		model.addAttribute("admtiPath", admtiPath);
		return "infoges/mensagem";
	}

	@GetMapping(value="/signout")
	public ResponseEntity<String> logout(HttpServletRequest req, HttpServletResponse res, ModelMap model){
		Cookie cookie = WebUtils.getCookie(req, "token");
		cookie.setValue(null);
		cookie.setMaxAge(0);
		cookie.setPath("/");
		res.addCookie(cookie);
		SecurityContextHolder.getContext().setAuthentication(null);
		return ResponseEntity.status(HttpStatus.OK).body(messages.get("config.intranet"));
	}
	
	@RequestMapping(value="/buscarDadosBancarios", method = RequestMethod.POST)
    public @ResponseBody Map<String, String> buscarDadosBancarios(@RequestBody SearchDTO searchDTO, HttpServletRequest request) {  
    
    	Favorecido favorecido = favorecidoService.getFavorecido(searchDTO);
    	
    	List<DadosBancarios> lst = dadosBancariosService.listaDadosBancarios(favorecido.getSegment1());
    	Map<String, String> dados = new HashMap<String, String>();
    	for(DadosBancarios banco : lst){
    		String descricao = String.format("Banco: %s, Agência: %s | Conta: %s", banco.getBanco(), banco.getAgencia(), banco.getConta());
    	    dados.put(banco.getBankAccountId(), descricao);
    	}
    	
    	request.setAttribute("dadosBancarios", lst);
    	
    	return dados;
    }
    
    
    @RequestMapping(value="/buscarValorExtenso", method = RequestMethod.GET )
    public @ResponseBody StringResponseDTO buscarValorExtenso(@RequestParam(value = "valor") String valor, HttpServletRequest request) {  
    	String retorno = lancamentoHeaderService.getExtenso(Double.valueOf(valor));
    	StringResponseDTO extenso = new StringResponseDTO(retorno);
    	return extenso;
     }

	@RequestMapping(value = "/buscarFavorecido", method = RequestMethod.POST)
    public @ResponseBody Favorecido buscarFavorecido(@RequestBody SearchDTO searchDTO, HttpServletRequest request) {        
    	
    	Favorecido favorecido = favorecidoService.getFavorecido(searchDTO);
    	
    	request.setAttribute("favorecido", favorecido);
    	
    	return favorecido;
    }
	
	@RequestMapping(value="/validaValorKM", method = RequestMethod.GET )
    public @ResponseBody Double buscarValorExtenso(@RequestParam(value = "tipo") Integer tipo, HttpServletRequest request) {  
    	Double valorKM = this.lancamentoLinesService.getValorKm(tipo);
    	return valorKM;
     }
    
    @RequestMapping(value="/validaCentroCusto", method = RequestMethod.GET )
    public @ResponseBody StringResponseDTO validaCentroCusto(@RequestParam(value = "tipo") Integer tipo, @RequestParam(value = "conta", required=false) Integer conta, @RequestParam(value = "centro") String centro,  @RequestParam(value = "chapa") String chapa, HttpServletRequest request) {  
    	
    	Favorecido favorecido = null;
    	
    	SearchDTO search = new SearchDTO();
    	search.setParametro(chapa);
    	search.setTipo(ID.getDescricao());
    	
    	favorecido = favorecidoService.getFavorecido(search);
    	
    	String retornofn = this.lancamentoLinesService.validaCentroCusto(Integer.valueOf(chapa), favorecido.getVendorSiteId(), favorecido.getOrgId(), tipo, null, centro, REEMBOLSO.getCodigo());
    	StringResponseDTO response = new StringResponseDTO(retornofn);
    	
    	return response;
     }
	
	private String getLogin(HttpServletRequest req, String login) throws IOException {

		if (!ambiente.equals(DESENVOLVIMENTO)) {
			try {
				login = req.getHeader("x-isrw-proxy-auth-user").split("\\\\")[1];
			} catch (Exception e) {
				login = req.getHeader("x-isrw-proxy-auth-user");
			}
		} else {
			login = usuarioDesenv;
		}

		return login;
	}
	
	private UsuarioDto createAdmTiUser(){
		UsuarioDto usuarioAdmti = new UsuarioDto();
		usuarioAdmti = new UsuarioDto();
		usuarioAdmti.setEmail("reembolso@sp.senac.br");
		usuarioAdmti.setCodSistema(codSistema);
		return usuarioAdmti;
	}
}