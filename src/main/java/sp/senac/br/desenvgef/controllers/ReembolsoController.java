package sp.senac.br.desenvgef.controllers;

import static sp.senac.br.desenvgef.enums.FormaPagamentoEnum.DEPOSITO;
import static sp.senac.br.desenvgef.enums.FormaPagamentoEnum.DINHEIRO;
import static sp.senac.br.desenvgef.enums.StatusPrestacaoEnum.ABERTO;
import static sp.senac.br.desenvgef.enums.TipoFavorecidoEnum.ID;
import static sp.senac.br.desenvgef.enums.TipoLancamentoEnum.REEMBOLSO;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import sp.senac.br.desenvgef.dtos.ReembolsoDTO;
import sp.senac.br.desenvgef.dtos.SearchDTO;
import sp.senac.br.desenvgef.dtos.UsuarioDto;
import sp.senac.br.desenvgef.entities.DadosBancarios;
import sp.senac.br.desenvgef.entities.Favorecido;
import sp.senac.br.desenvgef.entities.LancamentoHeader;
import sp.senac.br.desenvgef.entities.LancamentoLines;
import sp.senac.br.desenvgef.entities.TipoDespesa;
import sp.senac.br.desenvgef.services.DadosBancariosService;
import sp.senac.br.desenvgef.services.FavorecidoService;
import sp.senac.br.desenvgef.services.LancamentoHeaderService;
import sp.senac.br.desenvgef.services.LancamentoLinesService;
import sp.senac.br.desenvgef.services.TipoLancamentoService;



@Controller("/reembolso/reembolso")
@SessionAttributes({"usuario","menus"})
public class ReembolsoController {
	
	@Autowired
	private TipoLancamentoService tipoLancamentoService;
	
	@Autowired
	private LancamentoHeaderService lancamentoHeaderService;
	
	@Autowired
	private LancamentoLinesService lancamentoLinesService;
	
	@Autowired
	private FavorecidoService favorecidoService;
	
	@Autowired
	private DadosBancariosService dadosBancariosService;
	
	List<LancamentoLines> linhadespesas = new ArrayList<LancamentoLines>();
	
	
	@RequestMapping(value="/reembolso-form")
    public ModelAndView reembolsoPage(@ModelAttribute("reembolso") ReembolsoDTO reembolsoDTO, HttpServletRequest request, Model model) {
    	
    	ModelAndView mav = new ModelAndView("reembolso-page", "reembolso-entity", new ReembolsoDTO());
    	List<TipoDespesa> tipos = this.tipoLancamentoService.listaTipoReembolso();
    	List<TipoDespesa> contas = this.tipoLancamentoService.listaContaContabil();
        
    	reembolsoDTO.setTiposDeReembolso(tipos); 
    	reembolsoDTO.setContascontabeis(contas);
        model.addAttribute("reembolso", reembolsoDTO);
        mav.addObject("tiposMap", tipos);
        mav.addObject("contasMap", contas);
        
        return mav;
         
    }
	 
	@GetMapping(value="/reembolso-form/{id}")
	public ModelAndView find(@ModelAttribute("reembolso") ReembolsoDTO reembolsoDTO, @PathVariable Integer id, HttpServletResponse resp, ModelMap model){
		
		LancamentoHeader lancamento = this.lancamentoHeaderService.findOne(id); 
		linhadespesas = this.lancamentoLinesService.listaLinhasLancamento(id);
		
		Map<String, String> dados = new HashMap<String, String>();
		if(reembolsoDTO.getFormaPagamento().equals("Deposito")) {
			List<DadosBancarios> lst = reembolsoDTO.getDadosBancarios();
	    	for(DadosBancarios banco : lst){
	    		String descricao = String.format("Banco: %s, Agência: %s | Conta: %s", banco.getBanco(), banco.getAgencia(), banco.getConta());
	    	    dados.put(banco.getBankAccountId(), descricao);
	    	}	
		}		
		
		List<TipoDespesa> tipos = this.tipoLancamentoService.listaTipoReembolso();
		List<TipoDespesa> contas = this.tipoLancamentoService.listaContaContabil();
		reembolsoDTO.setIdLancamento(id);
		reembolsoDTO.setCodigoLancamento(lancamento.getCdLancamento());
		
		reembolsoDTO.setCentrocusto("");
		reembolsoDTO.setTipoReembolso("");
		reembolsoDTO.setNrdocumento("");
		reembolsoDTO.setQuantidade("");
		reembolsoDTO.setValorDespesa("");
		
		ModelAndView mav = new ModelAndView("reembolso-page", "reembolso-entity", reembolsoDTO);
		model.addAttribute("reembolso", reembolsoDTO);
		mav.addObject("tiposMap", tipos);
		mav.addObject("contasMap", contas);
		mav.addObject("lists", linhadespesas);		
		if(reembolsoDTO.getFormaPagamento().equals("Deposito"))
			mav.addObject("mapbank", dados);
		
		return mav;

	}
    
	@PostMapping(value="/process-reembolso")
	public String save(@ModelAttribute ReembolsoDTO reembolso, HttpServletRequest request, RedirectAttributes redirect){
    	
    	HttpSession session = request.getSession(false);
    	UsuarioDto usuario = null;
    	List<DadosBancarios> dadosBancarios = null;
    	
    	if (session != null) {
    		usuario = (UsuarioDto) session.getAttribute("usuario");
    	}
    	
    	LancamentoHeader lancamentoHeader = new LancamentoHeader();
		List<LancamentoLines> lancamentosLines = new ArrayList<LancamentoLines>();
    
		
		Favorecido favorecido = null;
		
		SearchDTO search = new SearchDTO();
    	search.setParametro(reembolso.getCodigoFavorecido());
    	search.setTipo(ID.getDescricao());
    	
    	favorecido = favorecidoService.getFavorecido(search);
		
    	lancamentoHeader = makeReembolso(reembolso, usuario, favorecido, lancamentoHeader, lancamentosLines);
    	
    	dadosBancarios = dadosBancariosService.listaDadosBancarios(favorecido.getSegment1());
    	reembolso.setDadosBancarios(dadosBancarios);
    	redirect.addFlashAttribute("reembolso", reembolso);
    	

    	return "redirect:/reembolso-form/" + lancamentoHeader.getIdLancamento();
	}

	private LancamentoHeader makeReembolso(ReembolsoDTO reembolso, UsuarioDto usuario, Favorecido favorecido, LancamentoHeader lancamentoHeader, List<LancamentoLines> lancamentosLines) {
		
		
		String valorNoMask = reembolso.getValorDespesa().replaceAll("\\.", "").replaceAll("\\,", ".");

		double valor = Double.parseDouble(valorNoMask);
		String codigo = this.lancamentoHeaderService.getCodigoLancamento(REEMBOLSO.getCodigo());
    	
		if (reembolso.getIdLancamento() != null && reembolso.getIdLancamento() != 0) {
    		
    		int ultimaLinha = 0;
    		int proximaLinha = 0;
    		
    		lancamentoHeader = this.lancamentoHeaderService.findOne(reembolso.getIdLancamento());
    		lancamentosLines = this.lancamentoLinesService.listaLinhasLancamento(reembolso.getIdLancamento());
    		
    		if(lancamentoHeader!= null) {
    			
    			ultimaLinha = lancamentoLinesService.getUltimaLinha(reembolso.getIdLancamento());
    			proximaLinha = ultimaLinha + 1;
    			
    			LancamentoLines linha = new LancamentoLines(lancamentoHeader, proximaLinha);
        		linha.setIdDespesa(Integer.valueOf(reembolso.getTipoReembolso()));
        		linha.setDtInicial(new Date());
        		linha.setDsDespesa(reembolso.getHistorico());
        		linha.setVlDespesa(valor);

        		linha.setCdCentroCusto(reembolso.getCentrocusto());
        		
        		if(reembolso.getQuantidade() != null && !reembolso.getQuantidade().isEmpty()){
        			linha.setQtDespesa(Integer.valueOf(reembolso.getQuantidade()));    		
        		}
        		
        		linha.setNrDoctoDespesa(reembolso.getNrdocumento());
        		linha.setDsDespesa(reembolso.getHistorico());

        		linha.setCdCentroCusto(reembolso.getCentrocusto());
        		if(reembolso.getQuantidade() != null && !reembolso.getQuantidade().isEmpty())
        			linha.setQtDespesa(Integer.valueOf(reembolso.getQuantidade()));    		
        		linha.setNrDoctoDespesa(reembolso.getNrdocumento());
        		linha.setDsDespesa(reembolso.getHistorico());

        		linha.setCdUsuIncl(String.valueOf(usuario.getChapa()));
        		linha.setCdUsuAlt(String.valueOf(usuario.getChapa()));
        		linha.setDtIncl(new Date());
        		linha.setDtAlt(new Date());
        		
        		lancamentosLines.add(linha);
        		lancamentoHeader.setLancamentoLines(lancamentosLines);
        		
        		lancamentoHeader.setIdTipoLancamento(REEMBOLSO.getCodigo());
        		lancamentoHeader.setDsLancamento(reembolso.getDetalhamento());
        		lancamentoHeader.setCdLancamento(codigo); 
        		lancamentoHeader.setDtLancamento(new Date());
        		
        		if(reembolso.getFormaPagamento().equals(DINHEIRO.getDescricao())){
        			lancamentoHeader.setIdFormaPagto(DINHEIRO.getCodigo());
        		} else {
        			lancamentoHeader.setIdFormaPagto(DEPOSITO.getCodigo());
        		}
        		
        		lancamentoHeader.setIdUnidOper(favorecido.getOrgId());

        		lancamentoHeader.setVlLancamento(valor);
        		lancamentoHeader.setNmFavorecido(favorecido.getVendorName()); 
        		lancamentoHeader.setNrDoctoFavorecido(favorecido.getSegment1()); 
        		lancamentoHeader.setIdSituacao(ABERTO.getCodigo()); 
        		lancamentoHeader.setVendorId(favorecido.getVendorId()); 
        		lancamentoHeader.setVendorSiteId(favorecido.getVendorSiteId()); 
        		lancamentoHeader.setCdUsuIncl(String.valueOf(usuario.getChapa()));
        		lancamentoHeader.setCdUsuAlt(null);
        		
        		if(reembolso.getContacorrente() != null && !reembolso.getContacorrente().isEmpty()) {
        			lancamentoHeader.setBankAccountId(Integer.valueOf(reembolso.getContacorrente()));
        		}else {
        			lancamentoHeader.setBankAccountId(null);
        		}
        		
        		lancamentoHeader = this.lancamentoHeaderService.save(lancamentoHeader);
    		}	
    	} else {
    		
    		LancamentoLines linha = new LancamentoLines(lancamentoHeader, 1);
    		linha.setIdDespesa(Integer.valueOf(reembolso.getTipoReembolso()));
    		linha.setDtInicial(new Date());
    		linha.setDsDespesa(reembolso.getHistorico());
    		linha.setVlDespesa(valor);

    		linha.setCdCentroCusto(reembolso.getCentrocusto());
    		
    		if(reembolso.getQuantidade() != null && !reembolso.getQuantidade().isEmpty()){
    			linha.setQtDespesa(Integer.valueOf(reembolso.getQuantidade()));    		
    		}
    		
    		linha.setNrDoctoDespesa(reembolso.getNrdocumento());
    		linha.setDsDespesa(reembolso.getHistorico());

    		linha.setCdUsuIncl(String.valueOf(usuario.getChapa()));
    		linha.setCdUsuAlt(null);		
    		lancamentosLines.add(linha);
    		lancamentoHeader.setLancamentoLines(lancamentosLines);    		
    		lancamentoHeader.setIdTipoLancamento(REEMBOLSO.getCodigo());
    		lancamentoHeader.setDsLancamento(reembolso.getDetalhamento());
    		lancamentoHeader.setCdLancamento(codigo); 
    		lancamentoHeader.setDtLancamento(new Date());
    		
    		if(reembolso.getFormaPagamento().equals(DINHEIRO.getDescricao())){
    			lancamentoHeader.setIdFormaPagto(DINHEIRO.getCodigo());
    		} else {
    			lancamentoHeader.setIdFormaPagto(DEPOSITO.getCodigo());
    		}
    		
    		lancamentoHeader.setIdUnidOper(favorecido.getOrgId());    		
    		lancamentoHeader.setVlLancamento(valor);
    		lancamentoHeader.setNmFavorecido(favorecido.getVendorName()); 
    		lancamentoHeader.setNrDoctoFavorecido(favorecido.getSegment1()); 
    		lancamentoHeader.setIdSituacao(ABERTO.getCodigo()); 
    		lancamentoHeader.setVendorId(favorecido.getVendorId()); 
    		lancamentoHeader.setVendorSiteId(favorecido.getVendorSiteId()); 
    		lancamentoHeader.setCdUsuIncl(String.valueOf(usuario.getChapa()));
    		lancamentoHeader.setCdUsuAlt(null);
    		
    		if(reembolso.getContacorrente() != null && !reembolso.getContacorrente().isEmpty()) {
    			lancamentoHeader.setBankAccountId(Integer.valueOf(reembolso.getContacorrente()));
    		}else {
    			lancamentoHeader.setBankAccountId(null);
    		}
    		
    		lancamentoHeader = this.lancamentoHeaderService.save(lancamentoHeader);
    	}

		
		return lancamentoHeader;
	}
}