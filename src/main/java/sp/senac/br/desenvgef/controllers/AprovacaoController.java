package sp.senac.br.desenvgef.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import sp.senac.br.desenvgef.dtos.AdiantamentoDTO;
import sp.senac.br.desenvgef.entities.TipoDespesa;
import sp.senac.br.desenvgef.services.TipoLancamentoService;

@Controller("/reembolso/aprovacao")
@SessionAttributes({"usuario","menus"})
public class AprovacaoController {
	
	@Autowired
	private TipoLancamentoService tipoLancamentoService;
	
	private static final String PAGE_HOME="home"; 
 
    @RequestMapping(value="/aprovacao-form")
    public ModelAndView adiantamentoPage() {
    	
    	ModelAndView mav = new ModelAndView("aprovacao-page", "aprovacao-entity", new AdiantamentoDTO());
    	List<TipoDespesa> tipos = this.tipoLancamentoService.listaTipoAdiantamentos();
    	List<TipoDespesa> diferenca = this.tipoLancamentoService.listaDiferencaAdiantamento();
    	
        mav.addObject("tiposMap", tipos);
        mav.addObject("diferencaMap", diferenca);
        
        return mav;
    }
}
