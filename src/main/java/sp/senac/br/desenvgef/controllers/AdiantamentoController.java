package sp.senac.br.desenvgef.controllers;

import static sp.senac.br.desenvgef.enums.FormaPagamentoEnum.DEPOSITO;
import static sp.senac.br.desenvgef.enums.FormaPagamentoEnum.DINHEIRO;
import static sp.senac.br.desenvgef.enums.StatusPrestacaoEnum.ABERTO;
import static sp.senac.br.desenvgef.enums.TipoLancamentoEnum.ADIANTAMENTO;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import sp.senac.br.desenvgef.dtos.AdiantamentoDTO;
import sp.senac.br.desenvgef.dtos.SearchDTO;
import sp.senac.br.desenvgef.dtos.UsuarioDto;
import sp.senac.br.desenvgef.entities.Favorecido;
import sp.senac.br.desenvgef.entities.LancamentoHeader;
import sp.senac.br.desenvgef.entities.LancamentoLines;
import sp.senac.br.desenvgef.entities.TipoDespesa;
import sp.senac.br.desenvgef.services.FavorecidoService;
import sp.senac.br.desenvgef.services.LancamentoHeaderService;
import sp.senac.br.desenvgef.services.TipoLancamentoService;

@Controller("/reembolso/adiantamento")
@SessionAttributes({"usuario","menus"})
public class AdiantamentoController {
	
	@Autowired
	private TipoLancamentoService tipoLancamentoService;
	
	@Autowired
	private LancamentoHeaderService lancamentoHeaderService;
	
	@Autowired
	private FavorecidoService favorecidoService;
	
	private static final String PAGE_HOME="home"; 
 
    @RequestMapping(value="/adiantamento-form")
    public ModelAndView adiantamentoPage() {
    	
    	ModelAndView mav = new ModelAndView("adiantamento-page", "adiantamento-entity", new AdiantamentoDTO());
    	List<TipoDespesa> tipos = this.tipoLancamentoService.listaTipoAdiantamentos();
    	List<TipoDespesa> diferenca = this.tipoLancamentoService.listaDiferencaAdiantamento();
    	
        mav.addObject("tiposMap", tipos);
        mav.addObject("diferencaMap", diferenca);
        
        return mav;
    }
    
    @PostMapping(value="/process-adiantamento")
	public String save(@ModelAttribute AdiantamentoDTO adiantamentoDTO, HttpServletRequest request){
    	
    	HttpSession session = request.getSession(false);
    	LancamentoHeader lancamentoHeader = makeAdiantamento(adiantamentoDTO, session, request);
		
		lancamentoHeader = this.lancamentoHeaderService.save(lancamentoHeader);

        return PAGE_HOME;
	}

	private LancamentoHeader makeAdiantamento(AdiantamentoDTO adiantamentoDTO, HttpSession session, HttpServletRequest request) {
		
		UsuarioDto usuario = null;
    	Favorecido favorecido = null;
    	if (session != null) {
    		usuario = (UsuarioDto) session.getAttribute("usuario");
    	}
    	
    	SearchDTO searchDTO = new SearchDTO();
    	searchDTO.setParametro(adiantamentoDTO.getCodigoFavorecido());
    	searchDTO.setTipo(adiantamentoDTO.getTipoBusca());
    	
    	favorecido = favorecidoService.getFavorecido(searchDTO);
    	
    	String codigo = this.lancamentoHeaderService.getCodigoLancamento(ADIANTAMENTO.getCodigo());
    	
    	LancamentoHeader lancamentoHeader = new LancamentoHeader();
		LancamentoLines linha = new LancamentoLines(lancamentoHeader, 1);
		List<LancamentoLines> lancamentosLines = new ArrayList<LancamentoLines>();
        
		String valorNoMask = adiantamentoDTO.getValor().replaceAll("\\.", "").replaceAll("\\,", ".");
		double valor = Double.parseDouble(valorNoMask);
		
		linha.setIdDespesa(Integer.valueOf(adiantamentoDTO.getTipoAdiantamento()));
		linha.setContaContabil(adiantamentoDTO.getTipodiferenca());
		
		linha.setDtInicial(new Date());
		linha.setDsDespesa(adiantamentoDTO.getDetalhamento()); 
		linha.setVlDespesa(valor);		
		linha.setCdUsuIncl(String.valueOf(usuario.getChapa()));
		linha.setCdUsuAlt(null);		
		lancamentosLines.add(linha);
		lancamentoHeader.setLancamentoLines(lancamentosLines);
		
		lancamentoHeader.setIdTipoLancamento(Integer.valueOf(adiantamentoDTO.getTipoAdiantamento()));
		lancamentoHeader.setDsLancamento(adiantamentoDTO.getDetalhamento());
		lancamentoHeader.setCdLancamento(codigo); 
		lancamentoHeader.setDtLancamento(new Date());
		
		if(adiantamentoDTO.getFormaPagamento().equals(DINHEIRO.getDescricao())){
			lancamentoHeader.setIdFormaPagto(DINHEIRO.getCodigo());
		} else {
			lancamentoHeader.setIdFormaPagto(DEPOSITO.getCodigo());
		}
		
		lancamentoHeader.setIdUnidOper(favorecido.getOrgId());
		lancamentoHeader.setVlLancamento(valor);
		lancamentoHeader.setNmFavorecido(favorecido.getVendorName()); 
		lancamentoHeader.setNrDoctoFavorecido(favorecido.getSegment1()); 
		lancamentoHeader.setIdSituacao(ABERTO.getCodigo()); 
		lancamentoHeader.setVendorId(favorecido.getVendorId()); 
		lancamentoHeader.setVendorSiteId(favorecido.getVendorSiteId()); 
		lancamentoHeader.setCdUsuIncl(String.valueOf(usuario.getChapa()));
		lancamentoHeader.setCdUsuAlt(null);
		
		if(adiantamentoDTO.getContacorrente() != null && !adiantamentoDTO.getContacorrente().isEmpty()) {
			lancamentoHeader.setBankAccountId(Integer.valueOf(adiantamentoDTO.getContacorrente()));
		}else {
			lancamentoHeader.setBankAccountId(null);
		}
		
		return lancamentoHeader;
	}
    
}
