/**
 * 
 */
package sp.senac.br.desenvgef.controllers;

import static net.sf.jasperreports.engine.JasperCompileManager.compileReport;
import static net.sf.jasperreports.engine.JasperExportManager.exportReportToPdfStream;
import static net.sf.jasperreports.engine.JasperFillManager.fillReport;
import static net.sf.jasperreports.engine.xml.JRXmlLoader.load;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;

/**
 * @author consultor.solucoes
 *
 */
@Controller
@RequestMapping
@SessionAttributes({"usuario", "menus", "favorecido", "dadosBancarios"})
public class RelatorioController {

	
	@PostMapping(value="/relatorio")
	public void imprimir(HttpServletResponse response) throws JRException, SQLException, IOException {
		
		Map<String, Object> parametros =  new HashMap<>();
		
		InputStream jasperStream = this.getClass().getResourceAsStream("/relatorios/adiantamento.jrxml");
	
		ByteArrayOutputStream baos =new ByteArrayOutputStream();
		response.setContentType("application/pdf");
		response.setHeader("Content-Disposition", "attachment; filename=adiantamento.pdf");
		
		JasperDesign design = load(jasperStream);

		JasperReport report = compileReport(design);
		JasperPrint print = fillReport(report, parametros);

		exportReportToPdfStream(print,baos);

		response.setContentLength(baos.size());
		ServletOutputStream out1 = response.getOutputStream();
		baos.writeTo(out1);
		out1.flush();
	    
	}
}
