package sp.senac.br.desenvgef.config;

import java.util.Locale;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Component;

@Component
public class Messages {

	@Autowired
	private MessageSource messageSource;

	private MessageSourceAccessor acessor;

	@PostConstruct
	private void init(){
		acessor = new MessageSourceAccessor(messageSource, new Locale("pt-BR"));
	}

	public String get(String code){
		return acessor.getMessage(code);
	}
}
