package sp.senac.br.desenvgef.dtos;

import java.io.Serializable;
import java.util.List;

import sp.senac.br.desenvgef.entities.TipoDespesa;

public class AdiantamentoDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Integer idLancamento;
	private String codigoLancamento;
	private String codigoFavorecido;
	private String favorecido;
	private String cpf;
	private String cnpj;
	private String documento; 
	private String tipoAdiantamento;
	private String detalhamento;
	private String valor;
	private String valorExtenso;
	private String formaPagamento;	
	private String contacorrente;
	private String tipodiferenca;
	private String tipoBusca;
	
	public String getTipoBusca() {
		return tipoBusca;
	}

	public void setTipoBusca(String tipoBusca) {
		this.tipoBusca = tipoBusca;
	}

	private List<TipoDespesa> tiposAdiantamento;
	private List<TipoDespesa> tiposDiferenca;
	
	
	public String getFavorecido() {
		return favorecido;
	}

	public void setFavorecido(String favorecido) {
		this.favorecido = favorecido;
	}

	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	public String getTipoAdiantamento() {
		return tipoAdiantamento;
	}

	public void setTipoAdiantamento(String tipoAdiantamento) {
		this.tipoAdiantamento = tipoAdiantamento;
	}

	public String getDetalhamento() {
		return detalhamento;
	}

	public void setDetalhamento(String detalhamento) {
		this.detalhamento = detalhamento;
	}

	public String getCodigoFavorecido() {
		return codigoFavorecido;
	}

	public void setCodigoFavorecido(String codigoFavorecido) {
		this.codigoFavorecido = codigoFavorecido;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public String getValorExtenso() {
		return valorExtenso;
	}

	public void setValorExtenso(String valorExtenso) {
		this.valorExtenso = valorExtenso;
	}
	
	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getFormaPagamento() {
		return formaPagamento;
	}

	public void setFormaPagamento(String formaPagamento) {
		this.formaPagamento = formaPagamento;
	}

	public String getContacorrente() {
		return contacorrente;
	}

	public void setContacorrente(String contacorrente) {
		this.contacorrente = contacorrente;
	}

	public Integer getIdLancamento() {
		return idLancamento;
	}

	public void setIdLancamento(Integer idLancamento) {
		this.idLancamento = idLancamento;
	}

	public String getCodigoLancamento() {
		return codigoLancamento;
	}

	public void setCodigoLancamento(String codigoLancamento) {
		this.codigoLancamento = codigoLancamento;
	}
	
	public List<TipoDespesa> getTiposAdiantamento() {
		return tiposAdiantamento;
	}

	public void setTiposAdiantamento(List<TipoDespesa> tiposAdiantamento) {
		this.tiposAdiantamento = tiposAdiantamento;
	}
	
	public String getTipodiferenca() {
		return tipodiferenca;
	}

	public void setTipodiferenca(String tipodiferenca) {
		this.tipodiferenca = tipodiferenca;
	}

	public List<TipoDespesa> getTiposDiferenca() {
		return tiposDiferenca;
	}

	public void setTiposDiferenca(List<TipoDespesa> tiposDiferenca) {
		this.tiposDiferenca = tiposDiferenca;
	}	
}