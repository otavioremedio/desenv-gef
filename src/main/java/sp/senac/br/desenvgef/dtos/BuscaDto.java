package sp.senac.br.desenvgef.dtos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class BuscaDto implements Serializable {
	private static final long serialVersionUID = 1L;

	private List<BuscaResultadoDto> suggestions = new ArrayList<>();

	public BuscaDto() {

	}

	public List<BuscaResultadoDto> getSuggestions() {
		return suggestions;
	}

	public void setSuggestions(List<BuscaResultadoDto> suggestions) {
		this.suggestions = suggestions;
	}

}
