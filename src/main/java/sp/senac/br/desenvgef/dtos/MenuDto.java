package sp.senac.br.desenvgef.dtos;

import java.io.Serializable;


public class MenuDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer codSistemaMenu;
	private Integer codSistema;
	private String nomeMenu;
	private String dscLink;
	private String menuPai;
	private String numPosTela;
	private Integer codSistemaPerfil;


	public MenuDto() {

	}


	public Integer getCodSistemaMenu() {
		return codSistemaMenu;
	}

	public void setCodSistemaMenu(Integer codSistemaMenu) {
		this.codSistemaMenu = codSistemaMenu;
	}

	public Integer getCodSistema() {
		return codSistema;
	}

	public void setCodSistema(Integer codSistema) {
		this.codSistema = codSistema;
	}

	public String getNomeMenu() {
		return nomeMenu;
	}

	public void setNomeMenu(String nomeMenu) {
		this.nomeMenu = nomeMenu;
	}

	public String getDscLink() {
		return dscLink;
	}

	public void setDscLink(String dscLink) {
		this.dscLink = dscLink;
	}

	public String getMenuPai() {
		return menuPai;
	}

	public void setMenuPai(String menuPai) {
		this.menuPai = menuPai;
	}

	public String getNumPosTela() {
		return numPosTela;
	}

	public void setNumPosTela(String numPosTela) {
		this.numPosTela = numPosTela;
	}

	public Integer getCodSistemaPerfil() {
		return codSistemaPerfil;
	}

	public void setCodSistemaPerfil(Integer codSistemaPerfil) {
		this.codSistemaPerfil = codSistemaPerfil;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codSistemaMenu == null) ? 0 : codSistemaMenu.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MenuDto other = (MenuDto) obj;
		if (codSistemaMenu == null) {
			if (other.codSistemaMenu != null)
				return false;
		} else if (!codSistemaMenu.equals(other.codSistemaMenu))
			return false;
		return true;
	}

}
