package sp.senac.br.desenvgef.dtos;

import java.io.Serializable;

public class SistemaParametroDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private String vlrSistemaParametro;
	private String nomeParametro;

	public SistemaParametroDto(){

	}

	public String getVlrSistemaParametro() {
		return vlrSistemaParametro;
	}

	public void setVlrSistemaParametro(String vlrSistemaParametro) {
		this.vlrSistemaParametro = vlrSistemaParametro;
	}

	public String getNomeParametro() {
		return nomeParametro;
	}

	public void setNomeParametro(String nomeParametro) {
		this.nomeParametro = nomeParametro;
	}


}
