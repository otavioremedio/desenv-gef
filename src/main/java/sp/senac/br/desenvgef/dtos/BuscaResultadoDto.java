package sp.senac.br.desenvgef.dtos;

import java.io.Serializable;

public class BuscaResultadoDto implements Serializable {
	private static final long serialVersionUID = 1L;

	private String value;
	private Integer data;

	public BuscaResultadoDto(String value, Integer data){
		this.value = value;
		this.data = data;
	}

	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public Integer getData() {
		return data;
	}
	public void setData(Integer data) {
		this.data = data;
	}




}
