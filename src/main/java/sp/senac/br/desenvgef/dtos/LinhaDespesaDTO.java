package sp.senac.br.desenvgef.dtos;

import java.io.Serializable;

public class LinhaDespesaDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	 
	private String tipoReembolso;	
	private String valor;		
	private String centrocusto;	
	private String nrdocumento;
	private String quantidade;
	
	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}
		
	public String getTipoReembolso() {
		return tipoReembolso;
	}

	public void setTipoReembolso(String tipoReembolso) {
		this.tipoReembolso = tipoReembolso;
	}

	public String getCentrocusto() {
		return centrocusto;
	}

	public void setCentrocusto(String centrocusto) {
		this.centrocusto = centrocusto;
	}
	
	public String getNrdocumento() {
		return nrdocumento;
	}

	public void setNrdocumento(String nrdocumento) {
		this.nrdocumento = nrdocumento;
	}

	public String getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(String quantidade) {
		this.quantidade = quantidade;
	}
}