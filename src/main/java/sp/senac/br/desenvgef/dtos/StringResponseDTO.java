package sp.senac.br.desenvgef.dtos;

import java.io.Serializable;

public class StringResponseDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;

    private String response;

    public StringResponseDTO(String s) { 
       this.setResponse(s);
    }

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

    // get/set omitted...
}