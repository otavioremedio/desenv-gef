package sp.senac.br.desenvgef.dtos;

import java.io.Serializable;

public class SearchDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String parametro;
	private String tipo;

	public String getParametro() {
		return parametro;
	}

	public void setParametro(String parametro) {
		this.parametro = parametro;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}	
}
