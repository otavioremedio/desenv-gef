package sp.senac.br.desenvgef.dtos;

import java.io.Serializable;
import java.util.List;

import sp.senac.br.desenvgef.entities.DadosBancarios;
import sp.senac.br.desenvgef.entities.TipoDespesa;

public class PrestacaoContaDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Integer idLancamento;
	private String codigoLancamento;
	private String codigoFavorecido;
	private String favorecido;
	private String cpf;
	private String cnpj;
	private String documento; 
	private String tipoReembolso;
	private String detalhamento;
	private String valorDespesa;
	private String formaPagamento;
	private String contacorrente;
	private String centrocusto;
	private String historico;
	private String nrdocumento;
	private String quantidade;
	private String contacontabil;
	private String codigoAdiantamento;
	

	private List<TipoDespesa> tiposDeReembolso;
	private List<TipoDespesa> contascontabeis;
	private List<DadosBancarios> dadosBancarios;
	
	
	public String getFavorecido() {
		return favorecido;
	}

	public void setFavorecido(String favorecido) {
		this.favorecido = favorecido;
	}

	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	public String getDetalhamento() {
		return detalhamento;
	}

	public void setDetalhamento(String detalhamento) {
		this.detalhamento = detalhamento;
	}

	public String getCodigoFavorecido() {
		return codigoFavorecido;
	}

	public void setCodigoFavorecido(String codigoFavorecido) {
		this.codigoFavorecido = codigoFavorecido;
	}

	
	
	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getFormaPagamento() {
		return formaPagamento;
	}

	public void setFormaPagamento(String formaPagamento) {
		this.formaPagamento = formaPagamento;
	}

	public String getContacorrente() {
		return contacorrente;
	}

	public void setContacorrente(String contacorrente) {
		this.contacorrente = contacorrente;
	}

	public String getTipoReembolso() {
		return tipoReembolso;
	}

	public void setTipoReembolso(String tipoReembolso) {
		this.tipoReembolso = tipoReembolso;
	}

	public String getCentrocusto() {
		return centrocusto;
	}

	public void setCentrocusto(String centrocusto) {
		this.centrocusto = centrocusto;
	}

	public String getHistorico() {
		return historico;
	}

	public void setHistorico(String historico) {
		this.historico = historico;
	}

	public String getNrdocumento() {
		return nrdocumento;
	}

	public void setNrdocumento(String nrdocumento) {
		this.nrdocumento = nrdocumento;
	}

	public String getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(String quantidade) {
		this.quantidade = quantidade;
	}

	public String getCodigoLancamento() {
		return codigoLancamento;
	}

	public void setCodigoLancamento(String codigoLancamento) {
		this.codigoLancamento = codigoLancamento;
	}

	public Integer getIdLancamento() {
		return idLancamento;
	}

	public void setIdLancamento(Integer idLancamento) {
		this.idLancamento = idLancamento;
	}

	public List<TipoDespesa> getTiposDeReembolso() {
		return tiposDeReembolso;
	}

	public void setTiposDeReembolso(List<TipoDespesa> tiposDeReembolso) {
		this.tiposDeReembolso = tiposDeReembolso;
	}

	public List<DadosBancarios> getDadosBancarios() {
		return dadosBancarios;
	}

	public void setDadosBancarios(List<DadosBancarios> dadosBancarios) {
		this.dadosBancarios = dadosBancarios;
	}

	public List<TipoDespesa> getContascontabeis() {
		return contascontabeis;
	}

	public void setContascontabeis(List<TipoDespesa> contascontabeis) {
		this.contascontabeis = contascontabeis;
	}

	public String getContacontabil() {
		return contacontabil;
	}

	public void setContacontabil(String contacontabil) {
		this.contacontabil = contacontabil;
	}

	public String getValorDespesa() {
		return valorDespesa;
	}

	public void setValorDespesa(String valorDespesa) {
		this.valorDespesa = valorDespesa;
	}
	
	public String getCodigoAdiantamento() {
		return codigoAdiantamento;
	}

	public void setCodigoAdiantamento(String codigoAdiantamento) {
		this.codigoAdiantamento = codigoAdiantamento;
	}	
}