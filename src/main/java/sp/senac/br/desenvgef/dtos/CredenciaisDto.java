package sp.senac.br.desenvgef.dtos;

import java.io.Serializable;

public class CredenciaisDto implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String username;
	private String senha;

	public CredenciaisDto() {
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
}
