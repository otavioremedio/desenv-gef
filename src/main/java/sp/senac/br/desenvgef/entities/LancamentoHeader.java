package sp.senac.br.desenvgef.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
//import javax.persistence.NamedNativeQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.Valid;

@Entity
@Table(name = "xxap_prest_cta_headers_all")
@SequenceGenerator(name = "seqLancamentoHeader", sequenceName = "xxap_prest_cta_headers_all_s", allocationSize = 1)
public class LancamentoHeader implements Serializable {

	private static final long serialVersionUID = 1L;


	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqLancamentoHeader")
	@Column(name = "id_lancamento")
	private Integer idLancamento;
	
	@Column(name = "id_tipo_lancamento")
	private Integer idTipoLancamento;
	
	@Column(name = "ds_lancamento")
	private String dsLancamento;
	
	@Column(name = "cd_lancamento")
	private String cdLancamento;
	
	@Column(name = "dt_lancamento")
	private Date dtLancamento;
	
	@Column(name = "id_forma_pagto")
	private Integer idFormaPagto;
	
	@Column(name = "id_unid_oper")
	private Integer idUnidOper;
	
	@Column(name = "vl_lancamento")
	private Double vlLancamento;
	
	@Column(name = "nm_favorecido")
	private String nmFavorecido;

	@Column(name = "nr_docto_favorecido")
	private String nrDoctoFavorecido;
	
	@Column(name = "id_situacao")
	private Integer idSituacao;
	
	@Column(name = "vendor_id")
	private Integer vendorId;

	@Column(name = "vendor_site_id")
	private Integer vendorSiteId;
	
	@Column(name = "dt_adiantamento")
	private Date dtAdiantamento;
	
	@Column(name = "vl_adiantamento")
	private Double vlAdiantamento;
	
	@Column(name = "vl_tot_despesa")
	private Double vlTotDespesa;
	
	@Column(name = "vl_dif_adiantamento")
	private Double vlDifAdiantamento;
	
	@Column(name = "bank_account_id")
	private Integer bankAccountId;
	
	@Column(name = "cd_adiantamento")
	private String cdAdiantamento;

	@Column(name = "cd_usu_incl")
	private String cdUsuIncl;

	@Column(name = "dt_incl")
	private Date dtIncl;

	@Column(name = "cd_usu_alt")
	private String cdUsuAlt;

	@Column(name = "dtAlt")
	private Date dtAlt;
	
	@OneToMany(fetch=FetchType.LAZY, cascade=CascadeType.ALL, mappedBy="id.lancamentoHeader")
	@Valid
	private List<LancamentoLines> lancamentoLines = new ArrayList<>();

	public List<LancamentoLines> getLancamentoLines() {
		return lancamentoLines;
	}

	public void setLancamentoLines(List<LancamentoLines> lancamentoLines) {
		this.lancamentoLines = lancamentoLines;
	}
	

	// metodo que executa antes do update
	@PreUpdate
	public void preUpdate() {
		dtAlt = new Date();
	}

	// metodo que executa antes do insert
	@PrePersist
	public void prePersist() {
		final Date atual = new Date();
		dtIncl = atual;
		//dtAlt = atual;
	}
	
	public Integer getIdLancamento() {
		return idLancamento;
	}

	public void setIdLancamento(Integer idLancamento) {
		this.idLancamento = idLancamento;
	}

	public Integer getIdTipoLancamento() {
		return idTipoLancamento;
	}

	public void setIdTipoLancamento(Integer idTipoLancamento) {
		this.idTipoLancamento = idTipoLancamento;
	}

	public String getDsLancamento() {
		return dsLancamento;
	}

	public void setDsLancamento(String dsLancamento) {
		this.dsLancamento = dsLancamento;
	}

	public String getCdLancamento() {
		return cdLancamento;
	}

	public void setCdLancamento(String cdLancamento) {
		this.cdLancamento = cdLancamento;
	}

	public Date getDtLancamento() {
		return dtLancamento;
	}

	public void setDtLancamento(Date dtLancamento) {
		this.dtLancamento = dtLancamento;
	}

	public Integer getIdFormaPagto() {
		return idFormaPagto;
	}

	public void setIdFormaPagto(Integer idFormaPagto) {
		this.idFormaPagto = idFormaPagto;
	}

	public Integer getIdUnidOper() {
		return idUnidOper;
	}

	public void setIdUnidOper(Integer idUnidOper) {
		this.idUnidOper = idUnidOper;
	}

	public Double getVlLancamento() {
		return vlLancamento;
	}

	public void setVlLancamento(Double vlLancamento) {
		this.vlLancamento = vlLancamento;
	}

	public String getNmFavorecido() {
		return nmFavorecido;
	}

	public void setNmFavorecido(String nmFavorecido) {
		this.nmFavorecido = nmFavorecido;
	}

	public String getNrDoctoFavorecido() {
		return nrDoctoFavorecido;
	}

	public void setNrDoctoFavorecido(String nrDoctoFavorecido) {
		this.nrDoctoFavorecido = nrDoctoFavorecido;
	}

	public Integer getIdSituacao() {
		return idSituacao;
	}

	public void setIdSituacao(Integer idSituacao) {
		this.idSituacao = idSituacao;
	}

	public Integer getVendorId() {
		return vendorId;
	}

	public void setVendorId(Integer vendorId) {
		this.vendorId = vendorId;
	}

	public Integer getVendorSiteId() {
		return vendorSiteId;
	}

	public void setVendorSiteId(Integer vendorSiteId) {
		this.vendorSiteId = vendorSiteId;
	}

	public Date getDtAdiantamento() {
		return dtAdiantamento;
	}

	public void setDtAdiantamento(Date dtAdiantamento) {
		this.dtAdiantamento = dtAdiantamento;
	}

	public Double getVlAdiantamento() {
		return vlAdiantamento;
	}

	public void setVlAdiantamento(Double vlAdiantamento) {
		this.vlAdiantamento = vlAdiantamento;
	}

	public Double getVlTotDespesa() {
		return vlTotDespesa;
	}

	public void setVlTotDespesa(Double vlTotDespesa) {
		this.vlTotDespesa = vlTotDespesa;
	}
	
	
	public Double getVlDifAdiantamento() {
		return vlDifAdiantamento;
	}

	public void setVlDifAdiantamento(Double vlDifAdiantamento) {
		this.vlDifAdiantamento = vlDifAdiantamento;
	}

	public Integer getBankAccountId() {
		return bankAccountId;
	}

	public void setBankAccountId(Integer bankAccountId) {
		this.bankAccountId = bankAccountId;
	}

	public String getCdAdiantamento() {
		return cdAdiantamento;
	}

	public void setCdAdiantamento(String cdAdiantamento) {
		this.cdAdiantamento = cdAdiantamento;
	}

	public String getCdUsuIncl() {
		return cdUsuIncl;
	}

	public void setCdUsuIncl(String cdUsuIncl) {
		this.cdUsuIncl = cdUsuIncl;
	}

	public Date getDtIncl() {
		return dtIncl;
	}

	public void setDtIncl(Date dtIncl) {
		this.dtIncl = dtIncl;
	}

	public String getCdUsuAlt() {
		return cdUsuAlt;
	}

	public void setCdUsuAlt(String cdUsuAlt) {
		this.cdUsuAlt = cdUsuAlt;
	}

	public Date getDtAlt() {
		return dtAlt;
	}

	public void setDtAlt(Date dtAlt) {
		this.dtAlt = dtAlt;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idLancamento == null) ? 0 : idLancamento.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LancamentoHeader other = (LancamentoHeader) obj;
		if (idLancamento == null) {
			if (other.idLancamento != null)
				return false;
		} else if (!idLancamento.equals(other.idLancamento))
			return false;
		return true;
	}

}
