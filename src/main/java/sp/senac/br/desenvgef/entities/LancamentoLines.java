package sp.senac.br.desenvgef.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

@Entity
@Table(name = "xxap_prest_cta_lines_all")
public class LancamentoLines implements Serializable {

	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private LancamentoPK id;

	public LancamentoLines() {  }

	public LancamentoLines(LancamentoHeader lancamentoHeader, Integer nrlinha){
		this.id = new LancamentoPK(lancamentoHeader, nrlinha);
	}

	public Integer getIdLancamento() { return id.getIdLancamento(); }

	public LancamentoPK getId() { return id; }

	public void setId(LancamentoPK id) { this.id = id; }
	

	@Column(name = "id_despesa_adiantamento")
	private Integer idDespesa;
	
	@Column(name = "dt_inicial")
	private Date dtInicial;

	@Column(name = "dt_final")
	private Date dtFinal;

	@Column(name = "ds_despesa")
	private String dsDespesa;

	@Column(name = "vl_despesa")
	private Double vlDespesa;

	@Column(name = "cd_centro_custo")
	private String cdCentroCusto;

	@Column(name = "qt_despesa")
	private Integer qtDespesa;

	@Column(name = "nr_docto_despesa")
	private String nrDoctoDespesa;

	@Column(name = "cd_usu_incl")
	private String cdUsuIncl;

	@Column(name = "dt_incl")
	private Date dtIncl;

	@Column(name = "cd_usu_alt")
	private String cdUsuAlt;

	@Column(name = "dt_alt")
	private Date dtAlt;
	
	@Column(name = "conta_contabil")
	private String contaContabil;
	
	@Column(name = "complemento")
	private String complemento;
	
	@Column(name = "code_combination_id")
	private Integer codeCombinationId;
	

	// metodo que executa antes do update
	@PreUpdate
	public void preUpdate() {
		dtAlt = new Date();
	}

	// metodo que executa antes do insert
	@PrePersist
	public void prePersist() {
		final Date atual = new Date();
		dtIncl = atual;
	}

	public Integer getIdDespesa() { return idDespesa; }
 
	public void setIdDespesa(Integer idDespesa) { this.idDespesa = idDespesa; }

	public Date getDtInicial() { return dtInicial; }

	public void setDtInicial(Date dtInicial) { this.dtInicial = dtInicial; }

	public Date getDtFinal() { return dtFinal; }

	public void setDtFinal(Date dtFinal) { this.dtFinal = dtFinal; }

	public String getDsDespesa() { return dsDespesa; }

	public void setDsDespesa(String dsDespesa) { this.dsDespesa = dsDespesa; }

	public Double getVlDespesa() { return vlDespesa; }

	public void setVlDespesa(Double vlDespesa) { this.vlDespesa = vlDespesa; }

	public String getCdCentroCusto() { return cdCentroCusto; }

	public void setCdCentroCusto(String cdCentroCusto) { this.cdCentroCusto = cdCentroCusto; }

	public Integer getQtDespesa() { return qtDespesa; }

	public void setQtDespesa(Integer qtDespesa) { this.qtDespesa = qtDespesa; }

	public String getNrDoctoDespesa() { return nrDoctoDespesa; }

	public void setNrDoctoDespesa(String nrDoctoDespesa) { this.nrDoctoDespesa = nrDoctoDespesa; }

	public String getCdUsuIncl() { return cdUsuIncl; }

	public void setCdUsuIncl(String idUsuIncl) { this.cdUsuIncl = idUsuIncl; }

	public Date getDtIncl() { return dtIncl; }

	public void setDtIncl(Date dtIncl) { this.dtIncl = dtIncl; }

	public String getCdUsuAlt() { return cdUsuAlt; }

	public void setCdUsuAlt(String cdUsuAlt) { this.cdUsuAlt = cdUsuAlt; }

	public Date getDtAlt() { return dtAlt; }

	public void setDtAlt(Date dtAlt) { this.dtAlt = dtAlt; }
	
	

	public String getContaContabil() {
		return contaContabil;
	}

	public void setContaContabil(String contaContabil) {
		this.contaContabil = contaContabil;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public Integer getCodeCombinationId() {
		return codeCombinationId;
	}

	public void setCodeCombinationId(Integer codeCombinationId) {
		this.codeCombinationId = codeCombinationId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LancamentoLines other = (LancamentoLines) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}	
}
