package sp.senac.br.desenvgef.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Embeddable
public class LancamentoPK implements Serializable {

	private static final long serialVersionUID = 1L;

	
	@Column(name = "nr_linha")
	private Integer nrLinha;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_lancamento")
	private LancamentoHeader lancamentoHeader;

	public LancamentoPK() {
	}

	public LancamentoPK(LancamentoHeader lancamentoHeader, Integer nrLinha){
		super();
		this.lancamentoHeader = lancamentoHeader;
		this.nrLinha = nrLinha;
	}

	public Integer getIdLancamento() {
		return lancamentoHeader.getIdLancamento();
	}

	public Integer getNrLinha() {
		return nrLinha;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((lancamentoHeader == null) ? 0 : lancamentoHeader.hashCode());
		result = prime * result + ((nrLinha == null) ? 0 : nrLinha.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LancamentoPK other = (LancamentoPK) obj;
		if (lancamentoHeader == null) {
			if (other.lancamentoHeader != null)
				return false;
		} else if (!lancamentoHeader.equals(other.lancamentoHeader))
			return false;
		if (nrLinha == null) {
			if (other.nrLinha != null)
				return false;
		} else if (!nrLinha.equals(other.nrLinha))
			return false;
		return true;
	}


}
