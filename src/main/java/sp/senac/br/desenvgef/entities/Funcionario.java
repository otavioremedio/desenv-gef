package sp.senac.br.desenvgef.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import com.google.common.base.Strings;

import sp.senac.br.desenvgef.enums.Ativo;
import sp.senac.br.desenvgef.enums.TipoColab;
import sp.senac.br.desenvgef.validation.FuncionarioInsert;

@Entity
@Table(name = "funcionarios")
@SequenceGenerator(name = "seqFunc", sequenceName = "rhev.s_funcionarios_01", allocationSize = 1)
@FuncionarioInsert
public class Funcionario implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqFunc")
	@Column(name = "id")
	private Integer id;

	@Column(name = "nome")
	@Length(min = 1, message = "Nome é obrigatório.")
	private String nome;

	@Column(name = "cic_nro")
	@NotNull(message = "Cpf é obrigatório.")
	private String cpf;

	@Column(name = "complemento_cic_nro")
	private String digCpf;

	@Column(name = "tipo_colab")
	private Character tipoColab;

	@Column(name = "ativo")
	private Character ativo;

	@Column(name = "rg_nro")
	@NotNull(message = "Rg é obrigatório.")
	private String rg;

	@Column(name = "rg_complemento")
	private String digRg;

	@Column(name = "uniorg_cgc")
	private String uniorgCgc;

	@Column(name = "cod_uniorg")
	private String codUniorg;

	@Column(name = "data_admissao")
	private final Date dataAdmissao = new Date();

	public Funcionario() {
		this.tipoColab = TipoColab.PRESTADOR.getCod();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		if (digCpf != null) {
			return Strings.padStart(cpf + digCpf, 11, '0');
		} else {
			if (cpf != null)
				return Strings.padStart(cpf, 11, '0');
		}

		return cpf;
	}

	public void setCpf(String cpf) {
		if (cpf != null && cpf.length() > 0) {
			this.cpf = cpf.substring(0, 9);
			this.digCpf = cpf.substring(9, 11);
		} else {
			this.cpf = null;
		}
	}

	public String getDigCpf() {
		return digCpf;
	}

	public void setDigCpf(String digCpf) {
		this.digCpf = digCpf;
	}

	public TipoColab getTipo() {
		if (tipoColab == null) {
			return TipoColab.PRESTADOR;
		}

		return TipoColab.toEnum(tipoColab);
	}

	public void setTipo(TipoColab tipoColab) {
		this.tipoColab = this.tipoColab;
	}

	public Ativo getAtivo() {
		if (ativo == null) {
			return Ativo.ATIVO;
		}

		return Ativo.toEnum(ativo);
	}

	public void setAtivo(Ativo ativo) {
		this.ativo = ativo.getCod();
	}

	public String getRg() {
		if (digRg != null) {
			return Strings.padStart(rg + digRg, 9, '0');
		} else {
			if (rg != null)
				return Strings.padStart(rg, 9, '0');
		}
		return rg;
	}

	public void setRg(String rg) {
		if (rg != null && rg.length() > 0) {
			this.rg = Strings.padStart(rg, 9, '0').substring(0, 8);

			if (rg.length() > 8) {
				this.digRg = rg.substring(8, 9);
			}
		} else {
			this.rg = null;
		}
	}

	public String getDigRg() {
		return digRg;
	}

	public void setDigRg(String digRg) {
		this.digRg = digRg;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Funcionario other = (Funcionario) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
