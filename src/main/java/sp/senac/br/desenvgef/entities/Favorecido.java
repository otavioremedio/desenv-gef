package sp.senac.br.desenvgef.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/*
select fav from xxvw_ap_app_prest_cta_h fav where 1=1 and tipo = 3 and employee_number= '464678' 
select * from xxvw_ap_app_prest_cta_h where 1=1 and tipo = 1 and segment1= '32938399839'
select * from xxvw_ap_app_prest_cta_h where 1=1 and tipo = 2 and segment1= '00288916001080' 
*/

@Entity
@Table(name="xxvw_ap_app_prest_cta_h")
public class Favorecido implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Column(name="EMPLOYEE_NUMBER")
	private String employeeNumber;
	
	@Column(name="VENDOR_NAME")
	private String vendorName;
	
	@Column(name="VENDOR_ID")
	private Integer vendorId;
	
	@Id
	@Column(name="SEGMENT1")
	private String segment1;
	
	@Column(name="VENDOR_SITE_ID")
	private Integer vendorSiteId;
	
	@Column(name="ORG_ID")
	private Integer orgId;
	
	@Column(name="TIPO")
	private String tipo;

	public String getEmployeeNumber() {
		return employeeNumber;
	}

	public void setEmployeeNumber(String employeeNumber) {
		this.employeeNumber = employeeNumber;
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public Integer getVendorId() {
		return vendorId;
	}

	public void setVendorId(Integer vendorId) {
		this.vendorId = vendorId;
	}

	public String getSegment1() {
		return segment1;
	}

	public void setSegment1(String segment1) {
		this.segment1 = segment1;
	}

	public Integer getVendorSiteId() {
		return vendorSiteId;
	}

	public void setVendorSiteId(Integer vendorSiteId) {
		this.vendorSiteId = vendorSiteId;
	}

	public Integer getOrgId() {
		return orgId;
	}

	public void setOrgId(Integer orgId) {
		this.orgId = orgId;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((segment1 == null) ? 0 : segment1.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Favorecido other = (Favorecido) obj;
		if (segment1 == null) {
			if (other.segment1 != null)
				return false;
		} else if (!segment1.equals(other.segment1))
			return false;
		return true;
	}
	
	
	
}
