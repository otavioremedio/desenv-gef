package sp.senac.br.desenvgef;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages={"sp.senac.br.desenvgef"})
@EnableAutoConfiguration
public class DesenvGefApplication extends SpringBootServletInitializer{

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(DesenvGefApplication.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(DesenvGefApplication.class, args);
    }
	
	
}
