package sp.senac.br.desenvgef.exceptions;

public class StandardException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public StandardException(String msg) {
		super(msg);
	}

	public StandardException(String msg, Throwable cause) {
		super(msg, cause);
	}

}
