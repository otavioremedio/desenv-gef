package sp.senac.br.desenvgef.exceptions;

import java.io.IOException;
import java.nio.file.AccessDeniedException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import sp.senac.br.desenvgef.config.Messages;

@ControllerAdvice
@SessionAttributes({"error"})
public class ControllerExceptionHandler {

	@Autowired
	Messages messages;

	@ExceptionHandler(DataIntegrityException.class)
	public ResponseEntity<StandardError> dataIntegrity(DataIntegrityException e, HttpServletRequest req) {

		StandardError error = new StandardError(System.currentTimeMillis(), HttpStatus.UNPROCESSABLE_ENTITY.value(),
				e.toString(), messages.get("exceptions.data.integrity.exception"), req.getRequestURI());
		return ResponseEntity.status(HttpStatus.OK).body(error);
	}

	@ExceptionHandler(SessionException.class)
	public ResponseEntity<StandardError> session(SessionException e, HttpServletRequest req) {

		StandardError error = new StandardError(System.currentTimeMillis(), HttpStatus.BAD_REQUEST.value(),
				e.toString(), messages.get("exceptions.session.exception"), req.getRequestURI());
		return ResponseEntity.status(HttpStatus.OK).body(error);
	}

	@ExceptionHandler(AccessDeniedException.class)
	public ModelAndView accessDenied(AccessDeniedException e, HttpServletRequest req) {
		StandardError error = new StandardError(System.currentTimeMillis(), HttpStatus.METHOD_NOT_ALLOWED.value(),
				e.toString(), messages.get("excpetions.access.denied.exception"), req.getRequestURI());

		ModelMap model = new ModelMap();
		model.addAttribute("error", error);
		return new ModelAndView("index", model);
	}

	@ExceptionHandler(SystemAccessDenied.class)
	public ModelAndView SystemAccessDenied(SystemAccessDenied e, HttpServletRequest req) {
		StandardError error = new StandardError(System.currentTimeMillis(), HttpStatus.FORBIDDEN.value(),
				e.toString(), messages.get("exceptions.system.access.denied"), req.getRequestURI());

		ModelMap model = new ModelMap();
		model.addAttribute("error", error);
		return new ModelAndView("index", model);
	}

	@ExceptionHandler(StandardException.class)
	public ModelAndView standard(StandardException e, HttpServletRequest req) {
		int HttpCode = HttpStatus.valueOf(Integer.valueOf(req.getAttribute("javax.servlet.error.status_code").toString())).value();
		Object message = req.getAttribute("javax.servlet.error.message");

		StandardError error = new StandardError(System.currentTimeMillis(),
				HttpCode > 0 ? HttpCode : HttpStatus.BAD_REQUEST.value(),
				null, (message == null || message.toString().length() == 0) ? "Erro: " + HttpStatus.NOT_FOUND.toString() + " - " + HttpStatus.NOT_FOUND.getReasonPhrase() : message.toString(), req.getRequestURI());
		ModelMap model = new ModelMap();
		model.addAttribute("error", error);
		return new ModelAndView("index", model);
	}

	@ExceptionHandler(IOException.class)
	public ModelAndView io(IOException e, HttpServletRequest req) {
		StandardError error = new StandardError(System.currentTimeMillis(), HttpStatus.BAD_REQUEST.value(),
				null, e.getMessage(), req.getRequestURI());
		ModelMap model = new ModelMap();
		model.addAttribute("error", error);
		return new ModelAndView("index", model);
	}

	@ExceptionHandler(DataIntegrityViolationException.class)
	public ModelAndView io(DataIntegrityViolationException e, HttpServletRequest req) {
		StandardError error = new StandardError(System.currentTimeMillis(), HttpStatus.BAD_REQUEST.value(),
				null, e.getCause().getCause().toString().replaceAll("\n", "").replaceAll("\"", "\'"), req.getRequestURI());
		ModelMap model = new ModelMap();
		model.addAttribute("error", error);
		return new ModelAndView("index", model);
	}

	@ExceptionHandler(Exception.class)
	public ModelAndView io(Exception e, HttpServletRequest req) {
		StandardError error = new StandardError(System.currentTimeMillis(), HttpStatus.BAD_REQUEST.value(),
				null, e.getMessage(), req.getRequestURI());
		ModelMap model = new ModelMap();
		model.addAttribute("error", error);
		return new ModelAndView("index", model);
	}

	@ExceptionHandler(BindException.class)
	public ModelAndView validation(BindException e, HttpServletRequest request) {

		ValidationError error = new ValidationError(System.currentTimeMillis(), HttpStatus.UNPROCESSABLE_ENTITY.value(), "Erro de validação", e.getMessage(), request.getRequestURI());
		for (FieldError x : e.getBindingResult().getFieldErrors()) {
			error.addError(x.getField(), x.getDefaultMessage());
		}

		ModelMap model = new ModelMap();
		model.addAttribute("error", error);
		return new ModelAndView("index", model);
	}

}
