package sp.senac.br.desenvgef.exceptions;

public class SessionException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public SessionException(String msg) {
		super(msg);
	}

	public SessionException(String msg, Throwable cause) {
		super(msg, cause);
	}

}
