package sp.senac.br.desenvgef.exceptions;

public class SystemAccessDenied extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public SystemAccessDenied(String msg) {
		super(msg);
	}

	public SystemAccessDenied(String msg, Throwable cause) {
		super(msg, cause);
	}
}

