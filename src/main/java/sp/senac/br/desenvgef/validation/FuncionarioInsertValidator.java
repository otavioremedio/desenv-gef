package sp.senac.br.desenvgef.validation;

import java.util.ArrayList;
import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import sp.senac.br.desenvgef.entities.Funcionario;
import sp.senac.br.desenvgef.exceptions.FieldMessage;
import sp.senac.br.desenvgef.utils.BR;

public class FuncionarioInsertValidator implements ConstraintValidator<FuncionarioInsert, Funcionario> {

	@Override
	public void initialize(FuncionarioInsert ann) {
	}

	@Override
	public boolean isValid(Funcionario funcionario, ConstraintValidatorContext context) {

		List<FieldMessage> list = new ArrayList<>();

		if (funcionario.getCpf() != null && !BR.isValidCPF(funcionario.getCpf())) {
			list.add(new FieldMessage("cpf", "CPF inválido."));
		}

		for (FieldMessage e : list) {
			context.disableDefaultConstraintViolation();
			context.buildConstraintViolationWithTemplate(e.getMessage()).addPropertyNode(e.getFieldName())
					.addConstraintViolation();
		}
		return list.isEmpty();
	}
}

