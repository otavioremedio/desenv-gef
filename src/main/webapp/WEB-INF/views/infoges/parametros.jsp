<%@include file="../../templates/header.jsp" %>
<section id="main-content" class="relative">
  <section class=" wrapper site-min-height">
    <div class="row">
      <div class="clearfix"></div>
      <div class="col-lg-12">
        <!-- Panel -->
        <div class="panel panel-default ">
          <!-- Topo -->
          <div class="panel-heading">
            <i class="fa fa-file hidden-inline-xs"></i>
            <spring:message code="views.cadastro.lbl.painel.admti" />
            <span class="tools pull-right"> 
            <a href="javascript:;" class="fa fa-chevron-down"
              aria-label="Minimizar" title="Minimizar"></a>
            </span>
          </div>
          <!-- Fim Topo -->
          <!-- panel-body -->
          <div class="panel-body">
            <!-- Linha Divisora -->
            <div class="clearfix"></div>
            <div class="line3"></div>
            <!-- Linha Divisora Fim -->
            <!--GRID -->
            <div class="row mg0">
              <iframe class="admti" src="${admtiPath}/include/administracao/parametros/index.jsp?codSistema=${idSistema}&include=yes&chapa=${usuario.chapa}"
              height="100%" width="100%" frameborder="0" marginwidth="5"></iframe>
            </div>
            <!-- Linha Divisora -->
            <div class="clearfix"></div>
            <div class="line3"></div>
            <!-- Linha Divisora Fim -->
          </div>
        </div>
      </div>
  </section>
</section>
<!-- end main-content-->
</section>
</body>
</html>