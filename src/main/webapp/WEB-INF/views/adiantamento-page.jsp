<%@include file="../templates/header.jsp"%>
<section id="main-content" class="relative">
	<section class=" wrapper site-min-height">
		<div class="panel panel-default">
			<!-- Topo -->
			<div class="panel-heading">
				<i class="fa fa-file-text hidden-inline-xs"></i>
				<spring:message code="views.adiantamento.lbl.header" />
				<span class="tools pull-right"></span>
			</div>
			<!-- Fim Topo -->
			<!-- panel-body -->
			<form:form  id="formAdiantamento" method="POST" commandName="adiantamento-entity" action="process-adiantamento">
				<div class="panel-body" style="">
					<div class="form-group"></div>
					<div class="container-fluid">
						<div class="form-row">
							<div class="col-md-2">
								<label for="tipoBusca">
									<spring:message code="views.adiantamento.lbl.tipobusca" /> 
								</label> 
								<form:select id="tipoBusca" path="tipoBusca" class="form-control">
									<option value="-" label="--Selecione" />
									<option><spring:message code="views.adiantamento.opt.codigo" /></option>
									<option><spring:message code="views.adiantamento.opt.cpf" /></option>
									<option><spring:message code="views.adiantamento.opt.cnpj" /></option>
								</form:select>
							</div>
							<div class="col-md-2" id="pnlcodigo">
								<form:label for="codigo" path="codigoFavorecido">
									<spring:message code="views.adiantamento.lbl.codigo" />
								</form:label>
								<form:input class="form-control" id="codigo" name="codigo" path="codigoFavorecido" placeholder="Chapa Favorecido"/>
							</div>
							<div class="col-md-2" id="pnlcpf">
								<form:label path="cpf">
									<spring:message code="views.adiantamento.lbl.cpf" />
								</form:label>
								<form:input class="form-control" id="cpf" name="cpf" path="cpf" placeholder="CPF Favorecido" />
							</div>
							<div class="col-md-2" id="pnlcnpj">
								<form:label path="cnpj">
									<spring:message code="views.adiantamento.lbl.cnpj" />
								</form:label>
								<form:input class="form-control" id="cnpj" name="cnpj" path="cnpj" placeholder="CNPJ Favorecido"/>
							</div>
							<div class="col-md-6 text-right">
								<button type="button" class="btn btn-primary .navbar-right" data-toggle="modal" id="btnBuscarFavorecido" title="" data-placement="center">
									<i class="fa fa-search"></i>
									<spring:message code="views.adiantamento.lbl.btn.search" />
								</button>
							</div>
							<div class="col-md-2">
								<form:label for="codigoLancamento" path="codigoLancamento">
									<spring:message code="views.adiantamento.lbl.cdlancamento" />
								</form:label>
								<form:input class="form-control" id="codigoLancamento" name="codigoLancamento" path="codigoLancamento" disabled="true" />
							</div>
						</div>
						<div class="form-row">
							<div class="col-md-6">
								<form:label path="favorecido">
									<spring:message code="views.adiantamento.lbl.favorecido" />
								</form:label>
								<form:input class="form-control" id="favorecido" name="favorecido" path="favorecido" placeholder="Favorecido" />
							</div>
							<div class="col-md-3">
								<form:label path="tipoAdiantamento">
									<spring:message code="views.adiantamento.lbl.tipoadiantamento" />
								</form:label>
								<form:select path="tipoAdiantamento" class="form-control" id="tipoAdiantamento" required="required">
									<form:option value="">
										<spring:message code="views.adiantamento.opt.tipoadiantamento" />
									</form:option>
									<form:options items="${tiposMap}" itemValue="codigo"
										itemLabel="descricao" />
								</form:select>
							</div>
							<div class="col-md-3">
								<form:label path="tipodiferenca">
									<spring:message code="views.adiantamento.lbl.tipodiferenca" />
								</form:label>
								<form:select path="tipodiferenca" class="form-control" id="tipodiferenca" disabled="true"> 
 									<form:option value=""> 
 										<spring:message code="views.adiantamento.opt.tipoadiantamento" /> 
 									</form:option> 
 									<form:options items="${diferencaMap}" itemValue="codigo" itemLabel="descricao" /> 
 								</form:select> 
							</div>
						</div>
						<div class="form-row">
							<div class="col-md-2">
								<form:label path="valor">
									<spring:message code="views.adiantamento.lbl.valor" />
								</form:label>
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="form-control" id="inputGroupPrepend"> 
											<spring:message code="views.adiantamento.lbl.moeda" />
										</span>
									</div>
									<form:input class="form-control" id="valor" name="valor" path="valor" placeholder="Valor da Adiantamento" aria-describedby="inputGroupPrepend" required="required" />
								</div>
							</div>
							<div class="col">
								<form:label path="valorExtenso">
									<spring:message code="views.adiantamento.lbl.valorextenso" />
								</form:label>
								<form:input class="form-control" id="valorExtenso" name="valorExtenso" path="valorExtenso" disabled="true" />
							</div>
						</div>
						<div class="form-row">
							<div class="col">
								<form:label path="detalhamento">
									<spring:message code="views.adiantamento.lbl.finalidade" />
								</form:label>
								<form:textarea style="height: 50px;" path="detalhamento" id="detalhamento" name="detalhamento" class="form-control" rows="5" cols="100" required="required" />
							</div>
						</div>
						<div class="form-row">
							<div class="col">
								<form:label path="formaPagamento">
									<spring:message code="views.adiantamento.lbl.formapagto" />
								</form:label>
								
								<form:radiobutton id="deposito" name="deposito" path="formaPagamento" value="Deposito" required="required"/>
								<spring:message code="views.adiantamento.opt.deposito" />
								
								<form:radiobutton id="dinheiro" name="dinheiro" path="formaPagamento" value="Dinheiro" required="required"/>
								<spring:message code="views.adiantamento.opt.dinheiro" />
								
							</div>
						</div>
						<div class="form-row" id="pnldadosBancarios">
							<div class="col">
								<label for="contacorrente">
									<spring:message code="views.adiantamento.lbl.dadosbancarios" />
								</label>
								<form:select class="form-control" id="dadosBancarios" name="dadosBancarios" items="${mapbank}" path="contacorrente" disabled="true"></form:select>
							</div>
						</div>
						<div class="form-group"></div>
						<div class="form-group"></div>
						<div class="container-fluid">
							<div class="row">
								<div class="col text-right">
									<input id="gravar" name="gravar" type="submit" value="Salvar Adiantamento" class="btn greenbtn">
								</div>
							</div>
						</div>
					</div>
					<div class="form-group"></div>
				</div>
		</div>

		</form:form>
	</section>
</section>
<%@include file="../templates/footer.jsp"%>