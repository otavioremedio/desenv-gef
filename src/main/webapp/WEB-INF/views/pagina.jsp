<%@include file="../templates/header.jsp"%>
<section id="main-content" class="relative">
	<section class=" wrapper site-min-height">
		<form class="needs-validation" novalidate>
			<div class="panel panel-default">
				<!-- Topo -->
				<div class="panel-heading">
					<i class="fa fa-file-text hidden-inline-xs"></i>Novos Lan�amentos<span
						class="tools pull-right"></span>
				</div>
				<!-- Fim Topo -->
				<!-- panel-body -->
				<form:form method="POST" action="/reembolso/saveAdiantamento" modelAttribute="funcionario" class="needs-validation" novalidate="novalidate">
				<div class="panel-body" style="">
					<div class="form-group"></div>
					<div class="form-row">
						<div class="form-check">
							<label for="exampleFormControlSelect2">Tipo Custo</label> 
<!-- 							<select -->
<!-- 								multiple class="form-control" id="exampleFormControlSelect2"> -->
<!-- 								<option>1 - default</option> -->
<!-- 								<option>2 - Despesa</option> -->
<!-- 								<option>3 - Despesa</option> -->
<!-- 								<option>4 - Despesa</option> -->
<!-- 								<option>5 - Despesa</option> -->
<!-- 							</select> -->
						
<%-- 						<spring:bind path="klantId">  --%>
<%-- 						<form:select path="klantId" items="${opdrachtGeversList}" itemValue="opdrachtGeverId" itemLabel="opdrachtGeverNaam"/>  --%>
<%-- 						</spring:bind> --%>

						</div>
						<div class="form-check">
							<label for="exampleFormControlSelect2">Centro Custo</label> <select
								multiple class="form-control" id="exampleFormControlSelect2">
								<option>1 - default</option>
								<option>2 - Centro Custo</option>
								<option>3 - Centro Custo</option>
								<option>4 - Centro Custo</option>
								<option>5 - Centro Custo</option>
								<option>6 - Centro Custo</option>
							</select>
						</div>
						<div class="col-md-4 mb-3">
							<label for="validationCustom01">Motivo Despesa</label> <input
								type="text" class="form-control" id="validationCustom01"
								placeholder="Motivo Despesa"
								aria-describedby="inputGroupPrepend" required>
							<div class="invalid-feedback">Esse campo � obrigat�rio!</div>
						</div>
						<div class="col-md-1.5 mb-1.5s">
							<label>Valor</label>
							<div class="input-group">
								<div class="input-group-prepend">
									<span class="form-control" id="inputGroupPrepend">R$</span>
								</div>

								<input type="text" class="form-control" id="validationCustom01"
									placeholder="Valor da Despesa"
									aria-describedby="inputGroupPrepend" required>
								<div class="invalid-feedback">Esse campo � obrigat�rio!</div>
							</div>
						</div>
					</div>
					<div class="form-check">
						<button class="btn btn-primary" type="submit">Salvar
							Despesa</button>
					</div>
				</div>
				</form:form>
				<div class="form-group"></div>
			</div>
			<script>
				// Example starter JavaScript for disabling form submissions if there are invalid fields
				(function() {
					'use strict';
					window.addEventListener('load', function() {
						// Fetch all the forms we want to apply custom Bootstrap validation styles to
						var forms = document
								.getElementsByClassName('needs-validation');
						// Loop over them and prevent submission
						var validation = Array.prototype.filter.call(forms,
								function(form) {
									form.addEventListener('submit', function(
											event) {
										if (form.checkValidity() === false) {
											event.preventDefault();
											event.stopPropagation();
										}
										form.classList.add('was-validated');
									}, false);
								});
					}, false);
				})();
			</script>
		</form>
	</section>
</section>
<%@include file="../templates/footer.jsp"%>