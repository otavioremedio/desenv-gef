<%@include file="../templates/header.jsp"%>
<section id="main-content" class="relative">
	<section class=" wrapper site-min-height">
		<div class="panel panel-default">
			<!-- Topo -->
			<div class="panel-heading">
				<i class="fa fa-file-text hidden-inline-xs"></i>
				<spring:message code="views.prestacao.lbl.header" />
				<span class="tools pull-right"></span>
			</div>
			<!-- Fim Topo -->
			<!-- panel-body -->
			<form:form id="formPrestacao" method="POST" commandName="prestacao-entity" action="process-prestacao">
				<form:hidden path="idLancamento" />
				<div class="panel-body" style="">
					<div class="form-group"></div>
					<div class="container-fluid">
						<div class="form-row">
							<div class="col-md-2">
								<label for="tipoBuscaPrest">
									<spring:message code="views.adiantamento.lbl.tipobusca" /> 
								</label> 
								<select class="form-control" id="tipoBuscaPrest" name="tipoBuscaPrest">
									<option value="-" label="--Selecione" />
									<option><spring:message code="views.adiantamento.opt.codigo" /></option>
									<option><spring:message code="views.adiantamento.opt.cpf" /></option>
									<option><spring:message code="views.adiantamento.opt.cnpj" /></option>
								</select>
							</div>
							<div class="col-md-2" id="pnlcodigo">
								<form:label for="codigo" path="codigoFavorecido">
									<spring:message code="views.adiantamento.lbl.codigo" />
								</form:label>
								<form:input class="form-control" id="codigo" name="codigo" path="codigoFavorecido" placeholder="Chapa Favorecido" />
							</div>
							<div class="col-md-2" id="pnlcpf">
								<form:label path="cpf">
									<spring:message code="views.adiantamento.lbl.cpf" />
								</form:label>
								<form:input class="form-control" id="cpf" name="cpf" path="cpf" placeholder="CPF Favorecido" />
							</div>
							<div class="col-md-2" id="pnlcnpj">
								<form:label path="cnpj">
									<spring:message code="views.adiantamento.lbl.cnpj" />
								</form:label>
								<form:input class="form-control" id="cnpj" name="cnpj" path="cnpj" placeholder="CNPJ Favorecido" />
							</div>

							<div class="col-md-6 text-right">
								<br/>
								<button type="button" class="btn btn-primary .navbar-right" data-toggle="modal" id="btnBuscarFavorecido" title="" data-placement="center">
									<i class="fa fa-search"></i>
									<spring:message code="views.reembolso.lbl.btn.search" />
								</button>
							</div>
							<div class="col-md-2">
								<form:label for="codigoLancamento" path="codigoLancamento">
									<spring:message code="views.prestacao.lbl.cdlancamento" />
								</form:label>
								<form:input class="form-control" id="codigoLancamento" name="codigoLancamento" path="codigoLancamento" disabled="true" />
							</div>
						</div>
						<div class="form-row">
							<div class="col-md-6">
								<form:label path="favorecido">
									<spring:message code="views.reembolso.lbl.favorecido" />
								</form:label>
								<form:input class="form-control" id="favorecido" name="favorecido" path="favorecido" required="required" disabled="true" />
							</div>
						</div>
						<div class="form-row">
							<div class="col-md-2">
								<form:label for="codigo" path="codigoAdiantamento">
									<spring:message code="views.prestacao.lbl.codigo.adiantamento" />
								</form:label>
								<form:input class="form-control" id="adiantamento" name="adiantamento" path="codigoAdiantamento" placeholder="C�digo do Adiantamento" />
							</div>
							<div class="col-md-2 text-right">
								<br/>
								<button type="button" class="btn btn-primary .navbar-right" data-toggle="modal" id="btnBuscarAdiatemento" title="" data-placement="center">
									<i class="fa fa-search"></i>
									<spring:message code="views.prestacao.lbl.btn.search.adiantento" />
								</button>
							</div>						
						</div>
						
						<div class="form-row">
							<div class="col">
								<form:label path="detalhamento">
									<spring:message code="views.reembolso.lbl.finalidade" />
								</form:label>
								<form:textarea style="height: 50px;" path="detalhamento" id="detalhamento" name="detalhamento" class="form-control" rows="5" cols="100" required="required" />
							</div>
						</div>
						<div class="form-row">
							<div class="col">
								<form:label path="formaPagamento">
									<spring:message code="views.reembolso.lbl.formapagto" />
								</form:label>
								<form:radiobutton id="deposito" name="deposito" path="formaPagamento" value="Deposito" required="required" />
								<spring:message code="views.reembolso.opt.deposito" />
								<form:radiobutton id="dinheiro" name="dinheiro" path="formaPagamento" value="Dinheiro" required="required" />
								<spring:message code="views.reembolso.opt.dinheiro" />
							</div>
						</div>
						<div class="form-row" id="pnldadosBancarios">
							<div class="col">
								<label for="contacorrente">
									<spring:message code="views.reembolso.lbl.dadosbancarios" />
								</label>
								<form:select class="form-control" id="dadosBancarios" name="dadosBancarios" items="${mapbank}" path="contacorrente" disabled="true">
								</form:select>
							</div>
						</div>
						<div class="form-row">
							<div class="col-md-2">
								<form:label path="tipoReembolso">
									<spring:message code="views.reembolso.lbl.tiporeembolso" />
								</form:label>
								<form:select path="tipoReembolso" class="form-control" id="tipoReembolso" required="required">
									<form:option value="">
										<spring:message code="views.reembolso.opt.tiporeembolso" />
									</form:option>
									<form:options items="${tiposMap}" itemValue="codigo" itemLabel="descricao" />
								</form:select>
							</div>
							<div class="col-md-2">
								<form:label path="contacontabil">
									<spring:message code="views.reembolso.lbl.tipoconta" />
								</form:label>
								<form:select path="contacontabil" class="form-control" id="contacontabil" disabled="true">
									<form:option value="">
										<spring:message code="views.reembolso.opt.tipoconta" />
									</form:option>
									<form:options items="${contasMap}" itemValue="codigo" itemLabel="descricao" />
								</form:select>
							</div>
							<div class="col-md-2" id="pnlCentroCusto">
								<form:label for="centrocusto" path="centrocusto">
									<spring:message code="views.reembolso.lbl.centrocusto" />
								</form:label>
								<form:input class="form-control" id="centrocusto" name="centrocusto" path="centrocusto" required="required" />
							</div>
							<div class="col-md-2">
								<form:label for="nrdocumento" path="nrdocumento">
									<spring:message code="views.reembolso.lbl.nrdocumento" />
								</form:label>
								<form:input class="form-control" id="nrdocumento" name="nrdocumento" path="nrdocumento" required="required" />
							</div>
							<div class="col-md-2">
								<form:label for="quantidade" path="quantidade">
									<spring:message code="views.reembolso.lbl.quantidade" />
								</form:label>
								<form:input class="form-control" id="quantidade" name="quantidade" path="quantidade" disabled="true" />
							</div>
							<div class="col-md-2">
								<form:label for="valorDespesa" path="valorDespesa">
									<spring:message code="views.reembolso.lbl.valor" />
								</form:label>
								<form:input class="form-control" id="valorDespesa" name="valorDespesa" path="valorDespesa" required="required" />
							</div>
						</div>
						<div class="form-group"></div>
						<div class="form-group"></div>
						<div class="container-fluid">
							<div class="row">
								<div class="col-md-8"></div>
								<div class="col-md-2 text-right">
									<button type="submit" id="adddespesa" name="adddespesa" class="btn btnheader greenbtn tooltips mobilebtn" data-toggle="modal" data-placement="top">
										<i class="fa fa-plus"></i>
										<spring:message code="views.reembolso.lbl.btn.adddespesa" />
									</button>
								</div>
								<div class="col-md-2 text-right">
									<button type="submit" id="salvar" name="salvar" class="btn btnheader greenbtn tooltips mobilebtn" data-toggle="modal" data-placement="top">
										<i class="fa fa-save"></i>
										<spring:message code="views.prestacao.lbl.btn.save" />
									</button>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group"></div>
				</div>
		</div>
		</form:form>
		<c:if test="${not empty lists}">
			<div class="panel-body" style="">
				<div class="col-lg-12 offset-0">
					<div class="table-responsive">
						<table
							class="table table-bordered table-hover table-striped sortable-theme-bootstrap "
							data-sortable="" data-sortable-initialized="true">
							<thead>
								<tr>
									<th class="small center sorting">Centro de Custo</th>
									<th class="small center sorting">N�mero Documento</th>
									<th class="small center sorting">Valor</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="item" items="${lists}">
									<tr>
										<td>${item.cdCentroCusto}</td>
										<td>${item.nrDoctoDespesa}</td>
										<td>${item.vlDespesa}</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</c:if>
	</section>
</section>
<%@include file="../templates/footer.jsp"%>