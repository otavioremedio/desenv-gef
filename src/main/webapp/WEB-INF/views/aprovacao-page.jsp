<%@include file="../templates/header.jsp"%>
<section id="main-content" class="relative">
	<section class=" wrapper site-min-height">
		<div class="row">
			<div class="col-md-12  margtop20 margbottom10">
				<!--
				<button class="btn btnheader greenbtn tooltips mobilebtn" data-toggle="modal" type="reset" id="btnNotaAdiantamento" title="" data-placement="top">
					<i class="fa fa-plus"></i>
					<spring:message code="views.cadastro.lbl.btn.adiantamento" />
				</button>
				<button class="btn btnheader greenbtn tooltips mobilebtn" data-toggle="modal" type="reset" id="btnPrestacaoConta" title="" data-placement="top">
					<i class="fa fa-plus"></i>
					<spring:message code="views.cadastro.lbl.btn.prestacao" />
				</button>
				<button class="btn btnheader greenbtn tooltips mobilebtn" data-toggle="modal" type="reset" id="btnReembolso" title="" data-placement="top">
					<i class="fa fa-plus"></i>
					<spring:message code="views.cadastro.lbl.btn.reembolso" />
				</button>
				  
				<button class="btn btnheader greenbtn tooltips mobilebtn" data-toggle="modal" type="reset" id="btnReembolso" title="" data-placement="top">
					<i class="fa fa-plus"></i>
					<spring:message code="views.cadastro.lbl.btn.reembolso" />
				</button>
				-->
				<form action="/reembolso/relatorio" method="POST">
					<input class="btn btnheader greenbtn tooltips mobilebtn" type="submit" value="Imprimir">
				</form>
			</div>
			<div class="col-lg-12">
				<!-- Panel -->
				<div class="panel panel-default">
					<!-- Topo -->
					<div class="panel-heading">
						<i class="fa fa-file-text hidden-inline-xs"></i>
						Lanšamentos Abertos
						<span class="tools pull-right"> 
							<a href="javascript:;" class="fa fa-chevron-down" aria-label="Minimizar" title="Minimizar"></a>
						</span>
					</div>
					<!-- Fim Topo -->
					<!-- panel-body -->
					<div class="panel-body" style="">
						<div class="col-lg-12 offset-0">
						<div class="table-responsive">
						<table
									class="table table-bordered table-hover table-striped sortable-theme-bootstrap "
									data-sortable="" data-sortable-initialized="true">
									<thead>
										<tr>
											<th class="small center sorting">Motivo</th>
											<th class="small center sorting">Detalhe 1</th>
											<th class="small center sorting">Detalhe 2</th>
											<th class="small center sorting">Detalhe 3</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="o" items="${lists}">
											<tr>
								                <td>${o}</td>
								                <td>${o}</td>
								                <td>${o}</td>
								                <td>${o}</td>
								            </tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
							<!-- / table responsive -->
						</div>
						<!-- /.col-lg-12 -->
					</div>
					<!-- /.panel-body -->
				</div>
				<!-- /.panel -->
			</div>
			<!-- /.col-lg-12 -->
		</div>
	</section>
</section>
<%@include file="../templates/footer.jsp"%>