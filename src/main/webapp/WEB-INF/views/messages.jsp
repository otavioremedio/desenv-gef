<%@page contentType="text/javascript" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
var messages = new Array();	

<c:forEach var="msg" items="${msgs}">
  messages["<spring:message text='${msg}' javaScriptEscape='true'/>"] = "<spring:message code='${msg}' javaScriptEscape='true' />";
</c:forEach>
function getMsg(key) {
    if (messages[key]) {
        return messages[key];
    }
    return key;
}
