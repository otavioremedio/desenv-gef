<!DOCTYPE html>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="pt">
<spring:message code="config.intranet" var="intranetPath"></spring:message>
<spring:message code="context.path" var="context"></spring:message>
<head>
  <base href="${context}" />
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <title><spring:message code="sistema.titulo" /></title>
  
  <!-- css da aplica��o -->
  <link href="css/form-validation.css" rel="stylesheet">
  <link href="css/reembolso.css" rel="stylesheet">
  
  <!-- Bootstrap / Font Awesome -->
  <link href="css/bootstrap.css" rel="stylesheet">
  <link href="css/font-awesome.css" rel="stylesheet" type="text/css">
  <link href="css/fontnormal.css" rel="stylesheet"> 
  <!-- Fonts -->
  <!-- Custom CSS -->
  <link href="css/custom_admin.css" rel="stylesheet">
  <!--  Css Separados  -->
  <link href="css/sets.css" rel="stylesheet">
  <link href="css/topo.css" rel="stylesheet">
  <link href="css/profile.css" rel="stylesheet">
  <link href="css/menu_esquerdo.css" rel="stylesheet">
  <link href="css/formulario.css" rel="stylesheet">
  <link href="css/tabelas.css" rel="stylesheet">
  <link href="css/img_geral.css" rel="stylesheet">
  <link href="css/accordion.css" rel="stylesheet">
  <link href="css/navtab.css" rel="stylesheet">
  <link href="css/modal.css" rel="stylesheet">
  <link href="css/botoes.css" rel="stylesheet">
  <link href="css/medias.css" rel="stylesheet">
  <link href="css/tema1.css" rel="stylesheet">
<!--   <link rel="stylesheet" type="text/css" href="chrome-extension://liecbddmkiiihnedobmlmillhodjkdmb/css/content.css"> -->
  
  <!-- CSS Switch Button -->
<!--   <link href="css/plugins/bootstrap-switch.css" rel="stylesheet"> -->
  <link href="css/plugins/datepicker.css" rel="stylesheet">
  <link href="css/plugins/daterangepicker-bs3.css" rel="stylesheet">
  <link rel="icon" href="img/favicon.png?v=5.8.6">
  
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script> 
      <![endif]-->
  
  <style>
  .cke {
  	visibility: hidden;
  }
  </style>
  <style>
	
/* Center the loader */
#loader {
   position: absolute;
  left: 50%;
  top: 50%;
  z-index: 1;
  width: 150px;
  height: 150px;
  margin: -75px 0 0 -75px;
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid #3498db;
  width: 120px;
  height: 120px;
  -webkit-animation: spin 2s linear infinite;
  animation: spin 2s linear infinite;
}

@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}

/* Add animation to "page content" */
.animate-bottom {
  position: relative;
  -webkit-animation-name: animatebottom;
  -webkit-animation-duration: 1s;
  animation-name: animatebottom;
  animation-duration: 1s
}

@-webkit-keyframes animatebottom {
  from { bottom:-100px; opacity:0 } 
  to { bottom:0px; opacity:1 }
}

@keyframes animatebottom { 
  from{ bottom:-100px; opacity:0 } 
  to{ bottom:0; opacity:1 }
}


	</style>
  <!-- Jquery Biblioteca-->
  <script src="js/jquery-3.2.0.js"></script>
  <!-- Bootstrap Core JavaScript -->
  <script src="js/popper.min.js" charset="utf-8"></script>
  <script src="js/bootstrap.min.js"></script>
  <!-- Bootstrap Core JavaScript -->
  <!-- Temas -->
  <script src="js/themes.js"></script>
  <script src="js/fontsize.js"></script>
  <script src="js/scripts.js"></script>
  <!-- JavaScript Custom para Todas as P�ginas -->
  <script src="js/scripts_custom.js"></script>
  <!-- Js accordion  Menu -->
  <script class="include" type="text/javascript" src="js/plugins/jquery.dcjqaccordion.2.7.js"></script>
  <!-- Js Scroll Personalizado Menu -->
  <script src="js/plugins/jquery.nicescroll.js"></script>
  
  <!-- Switch Button    -->
  <script src="js/plugins/highlight.js"></script>
<!--     <script src="js/plugins/bootstrap-switch.js"></script> -->
<!--   <script src="js/plugins/main.js"></script> -->
  <script src="js/plugins/bootstrap-datepicker.js"></script>
  <script src="js/plugins/bootstrap-datetimepicker.js"></script>
  <script src="js/plugins/datepicker_custom.js"></script>
  <script src="js/jquery.mask.js"></script>
  
  <!-- adicionar scripts especificos da aplica��o -->
  <script src="js/jquery.autocomplete.js" charset="utf-8"></script>
  <script type="text/javascript">
  	var noSuggestionNoticeVal = "<spring:message code='cadastro.js.no.results.autocomplete' javaScriptEscape='true'/>";
  </script>
  <script src="js/cadastro.js" charset="utf-8"></script>
  <script src="js/validacao.js" charset="utf-8"></script>
  <script src="js/header.js" charset="utf-8"></script>  
</head>
<body>
  <input type="hidden" id="context" value="${context}"/>
  <input type="hidden" id="intranet" value="${intranetPath}" />
  <!-- ====================== -->
  <section id="container">
    <!--header start-->
    <header class="header fixed-top clearfix" id="header_top">
      <!--logo start-->
      <div class="brand">
        <!--logo start-->
        <a href="${context}" title="Logo Senac" class="navbar-brand logo"><img
          src="img/logo.png" alt="Logo Senac"></a>
        <!--logo end-->
        <div class="sidebar-toggle-box">
          <div class="fa fa-bars" title="Minimizar/Maximizar Menu"></div>
        </div>
      </div>
      <!--logo end-->
      <!-- profile -->
      <div class="top-nav clearfix">
        <ul class="nav pull-right top-menu">
          <!-- user login dropdown start-->
          <li class="dropdown"><a data-toggle="dropdown"
            class="dropdown-toggle" href=""
            title="Nome do Usu�rio logado - Menu de Configura��o"> <img
              alt="" src="img/avatar1_small.jpg"> <span
              class="username hidden-inline-xs">${usuario.nmUsuario}<b class="caret"></b>
            </span></a>
            <ul class="dropdown-menu extended logout">
              <li><a title="Perfil"><i
                  class="fa fa-user"></i>${usuario.perfil.descricaoFront}</a></li>
              <li><a id="lnkSair" title="Sair"><i
                  class="fa fa-sign-out" aria-hidden="true"></i><spring:message code="views.header.lbl.btn.sair"/></a></li>
              <li class="divider"></li>
<!--               <li> -->
<!--                 <div class="center" title="Escolha o Tema"> -->
<!--                   <span class="mt-5"> Temas </span> -->
<!--                   <div class="clearfix"></div> -->
<!--                   <a href="#" data-theme="default" -->
<!--                     class="theme-link btn btn-white" title="Tema Dark"> -->
<!--                     <i class="fa fa-square dark" aria-hidden="true"></i> -->
<!--                   </a> <a href="#" data-theme="orange" -->
<!--                     class="theme-link btn btn-white" title="Tema Orange"> -->
<!--                     <i class="fa fa-square lorange" aria-hidden="true"></i> -->
<!--                   </a> <a href="#" data-theme="blue" -->
<!--                     class="theme-link btn btn-white" title="Tema Blue"> -->
<!--                     <i class="fa fa-square lblue" aria-hidden="true"></i> -->
<!--                   </a> -->
<!--                 </div> -->
<!--               </li> -->
            </ul></li>
          <!-- user login dropdown end -->
        </ul>
      </div>
      <!-- profile end -->
    </header>
    <!--header end-->

    <!--Menu Principal -->
    <aside>
      <!-- sidebar menu start-->
      <div id="sidebar" class="nav-collapse navbar-inverse">
        <div class="leftside-navigation" tabindex="5000" style="overflow: hidden; outline: none;">
          <ul class="sidebar-menu" id="nav-accordion" title="Menu Principal">
            <c:set var="fechar" scope="session" value="false"></c:set>
            <c:forEach var="menu" items="${menus}">
              <c:choose>
                <c:when test="${menu.menuPai == null && menu.dscLink != null}">
                  <li class="sub-menu dcjq-parent-li">
                    <a href="${menu.dscLink}" class="dcjq-parent" title="Atendimento"> 
                      <i class="${menu.dscLink != '.'?'fa fa-play':'fa fa-home'}"></i><span>${menu.nomeMenu}</span><span class="dcjq-icon"></span>
                    </a>
                  </li>
                </c:when>
                <c:when test="${menu.menuPai == null && menu.dscLink == null}">
                  <c:if test="${fechar == true}">
                    </ul>
                    </li>
                    <c:set var="fechar" scope="session" value="false"></c:set>
                  </c:if>
                  <li class="sub-menu dcjq-parent-li">
                    <a href="javascript:;" class="dcjq-parent" title="${menu.nomeMenu}"> 
                      <i class="fa fa-play"></i><span>${menu.nomeMenu}</span><span class="dcjq-icon"></span>
                    </a>
                    <ul class="sub" title="Submenu de ${menu.nomeMenu}">                  
                  <c:set var="fechar" scope="session" value="true"></c:set>
                </c:when>
                <c:otherwise>
                  <li><a href="${menu.dscLink}" title="${menu.nomeMenu}"><i class="fa fa-angle-right"></i>${menu.nomeMenu}</a></li>
                </c:otherwise>
              </c:choose>
            </c:forEach>
          </ul>
        </div>
      </div>
      <!-- sidebar menu end-->
    </aside>
    <!-- fim do cabe�alho padr�o -->
    <div class="text-center" hidden>
      <!-- Button HTML (to Trigger Modal) -->
      <button type="button" href="#modalAlert" id="btnModalAlert" data-backdrop="static" data-keyboard="false" class="trigger-btn" data-toggle="modal"></button>
    </div>
    <!-- Modal HTML -->
    <div class="modal fade" id="modalAlert" tabindex="-1" role="dialog" aria-labelledby="modalAlert" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title"><spring:message code="sistema.nome"/></h4>
          </div>
          <div class="modal-body"></div>
          <div class="modal-footer">
            <button class="btn btn-danger" type="button"><spring:message code="views.header.lbl.btn.modal"/></button>
          </div>
        </div>
      </div>
    </div>
    <div class="text-center" hidden>
      <!-- Button HTML (to Trigger Modal) -->
      <button type="button" href="#modalConfirm" id="btnModalConfirm" data-backdrop="static" data-keyboard="false" class="trigger-btn" data-toggle="modal"></button>
    </div>
    <div class="modal fade" id="modalConfirm" tabindex="-1" role="dialog" aria-labelledby="modalConfirm" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title"><spring:message code="sistema.nome"/></h4>
          </div>
          <div class="modal-body"></div>
          <div class="modal-footer">
            <button data-dismiss="modal" class="btn btn-default" type="button"><spring:message code="views.cadastro.lbl.btn.modal.fechar"/></button>
            <button class="btn btn-warning" type="button"><spring:message code="views.cadastro.lbl.btn.modal.confirmar"/></button>
          </div>
        </div>
      </div>
    </div> 
    <div id="loader" class="loader"></div>   
    <c:if test="${error != null}">
      <c:set var="errorMessage" value="${error.status},${error.message}"/>
      <spring:message code="views.header.modal.title.erro" var="titleErrorMessage"></spring:message>
      <spring:message code="exceptions.msg.chamado" var="msgChamado"></spring:message>      
      <c:choose>
        <c:when test="${error.status == 403 || error.status == 405}">
          <script type="text/javascript">accessDenied('<c:out value="${errorMessage}"/>','<c:out value="${titleErrorMessage}"/>');</script>    
        </c:when>
        <c:when test="${error.status == 422}">
          <script type="text/javascript">
            var errors = [];
            <c:forEach var="error" items="${error.errors}">
            	errors.push('<c:out value='${error.message}' />');
            </c:forEach>
            
            validationError(errors, '<c:out value="${titleErrorMessage}"/>');
          </script>    
        </c:when>
        <c:otherwise>
          <script type="text/javascript">showError('<c:out value="${errorMessage}"/>','<c:out value="${titleErrorMessage}"/>', '<c:out value="${msgChamado}"/>');</script>    
        </c:otherwise>
      </c:choose>
    </c:if>
    <c:if test="${sucesso}">
      <spring:message code="views.cadastro.txt.modal.sucesso" var="msgSucesso"></spring:message>
      <script type="text/javascript">showSuccess('<c:out value="${msgSucesso}"/>');</script>    
    </c:if>
    <br />
    <br />