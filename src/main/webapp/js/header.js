/**
 * http://usejsdoc.org/
 */

$(document).ready(function(){
	
	$('#lnkSair').click(function(){
		$.get('/reembolso/signout', function(data){
			window.location = data;
		}); 
	});
	
	var inteval = setInterval(getSession, 5000);
	function getSession() {
		
		if($('#modalAlert').css('display') == 'none'){
			$.get('/reembolso/session', function(data) {
			}).done(function(error) {
				if(error !== 'ok'){
					clearInterval(inteval);
					$('#modalAlert .modal-body').text(error.message);
					$('#btnModalAlert').click();
					$('#modalAlert .btn').click(function() {
						window.location = $('#intranet').val();
					});
				}
			}).fail(function(error) {
					clearInterval(inteval);
					$('#modalAlert .modal-body').text(error.message);
					$('#btnModalAlert').click();
					$('#modalAlert .btn').click(function() {
						window.location = $('#intranet').val();
					});
			});
		}		
	}
	
	//getSession();
});

function accessDenied(error, title) {
	$('.modal-title').text(title);
	$('#modalAlert .btn').removeClass('btn-success');
	$('#modalAlert .btn').addClass('btn-danger');
	$('#modalAlert .modal-body').text(error.split(',')[1]);
	$('#btnModalAlert').click();
	$('#modalAlert .btn').click(function() {
		var status = error.split(',')[0];
		if (status === '405')
			redirectHome();
		else
			redirectIntra();
	});
}

function redirectHome() {
	window.location = '/reembolso';
}

function redirectIntra() {
	window.location = $('#intranet').val();
}

function showError(error, title, msg) {
	$('.modal-title').text(title);
	var status = error.split(',')[0];
	if(status != '404' && status != '401'){
		$('#modalAlert .modal-body').text(msg + '\n\n' + error.split(',')[1].replace(/&#039;/g,'\''));
	} else {
		$('#modalAlert .modal-body').text(error.split(',')[1].replace(/&#039;/g,'\''));
	}
	$('#modalAlert .btn').removeClass('btn-success');
	$('#modalAlert .btn').addClass('btn-danger');
	$('#btnModalAlert').click();
	$('#modalAlert .btn').click(function() {
		redirectHome();		
	});
}

function validationError(errors, title) {
	$('.modal-title').text(title);
	var msgError = '';
	
	errors.forEach(function(m){
		msgError += m + '\n';
	});
	
	$('#modalAlert .modal-body').text(msgError);
	$('#modalAlert .btn').removeClass('btn-success');
	$('#modalAlert .btn').addClass('btn-danger');
	$('#btnModalAlert').click();
	$('#modalAlert .btn').click(function() {
		redirectHome();		
	});
}
