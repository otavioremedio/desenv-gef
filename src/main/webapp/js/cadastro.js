(function() {
	'use strict';
	window.addEventListener('load', function() {
		var forms = document.getElementsByClassName('needs-validation');
		var validation = Array.prototype.filter.call(forms, function(form) {
			form.addEventListener('submit', function(event) {
				if (form.checkValidity() === false) {
					event.preventDefault();
					event.stopPropagation();
				} else {
					$('.cpf').unmask();
					$('.rg').unmask();
				}
				form.classList.add('was-validated');
			}, false);
		});
	}, false);
})();

$(document).ready(function() {
	
	$('#loader').hide();  
	
	$('#tipoReembolso').on('change', function() {
		
		//KM
		if(this.value === "1"){
			$('#contacontabil').attr('disabled', true);
			$('#quantidade').attr('disabled', false);
			$('#valorDespesa').attr('disabled', true);
		}else{
			//OUTROS
			if(this.value === "12"){			
				$('#contacontabil').attr('disabled', false);
				$('#quantidade').attr('disabled', true);
				$('#valorDespesa').attr('disabled', false);
			}else{
				$('#quantidade').attr('disabled', true);
				$('#contacontabil').attr('disabled', true);
				$('#valorDespesa').attr('disabled', false);
			}
		}		
	});
	
	//diferença de adiantamento
	$('#tipoAdiantamento').on('change', function() {
		if(this.value === "5"){
			$('#tipodiferenca').attr('disabled', false);
		}else{
			$('#tipodiferenca').attr('disabled', true);
		}	
	});
	
	//Valida centro de custo
	$('#centrocusto').blur(function() {		
		var param = {};
		var conta = $('#contacontabil').val();
		if(conta === ""){
			param = {
				'tipo': $('#tipoReembolso').val(),
				'centro': this.value,
				'chapa': $('#codigo').val()
			}
		}else{
			param = {
				'tipo': $('#tipoReembolso').val(),
				'conta': conta,
				'chapa': $('#codigo').val(),
				'centro': this.value
			}
		}
		
		$.ajax({
			type : 'GET',
            url: 'validaCentroCusto',
            data : param,
            dataType: 'json',
            beforeSend: function() {
            	$("#loader").show();
            },
            success: function(data) { 
            	if(data.response !== null) {
                    if(data.response.length > 0) {
                        if (!isNaN(data.response)) {
                            //TODO:GRAVAR NO HIDDEN
                        	$('#gravar').attr('disabled', false);
                        	$('#codeCombinationId').attr('value', data.response);
                        }else{
                        	showAlert(data.response);
                        	$('#gravar').attr('disabled', true);
                        }
                    }
                }
            	
                $("#loader").hide();  
            },
            error: function(){      
            	$("#loader").hide();
			}
        });		
	});
	
	//calcula o valor do KM
	$('#quantidade').blur(function() {		
		var param = {
			'tipo': $('#tipoReembolso').val()
		}
		$.ajax({
			type : 'GET',
            url: 'validaValorKM',
            data : param,
            dataType: 'json',
            beforeSend: function() {
            	$("#loader").show();
            },
            success: function(data) {                
            	var qtd = $('#quantidade').val();
            	var total = qtd * data;
            	$("#valorDespesa").val(total);
            	
                $("#loader").hide();  
            },
            error: function(){      
            	$("#loader").hide();
			}
        });		
	});
	
	//recupera o valor extenso com base no valor digitado
	$('#valor').blur(function() {
		var v = $('#valor').val();
		var v1 = v.replace(/\./g, '');
		var valor = v1.replace(/\,/g, '');
		
		var param = {
			'valor': valor
		}
		
		$.ajax({
			type : 'GET',
            url: 'buscarValorExtenso',
            data : param,
            dataType: 'json',
            beforeSend: function() {
            	$("#loader").show();
            },
            success: function(data) {                
            	$('#valorExtenso').val(data.response);
                $("#loader").hide();  
            },
            error: function(){      
            	$("#loader").hide();
			}
        });
	});

	//inicializa desabilitado e habilita ao buscar o favorecido
	//adiantamento
	$('#favorecido').attr('disabled', true);

	//*************** disabled nao post os valores no submit  
	$('#tipoAdiantamento').attr('disabled', true);
	$('#valor').attr('disabled', true);
	$('#detalhamento').attr('disabled', true);
    
    //reembolso
    if ($('#codigoLancamento').val()) {
    	$('#tipoReembolso').attr('disabled', false);
        $('#centrocusto').attr('disabled', false);
        $('#nrdocumento').attr('disabled', false);
        $('#valorDespesa').attr('disabled', false);
        $('#dinheiro').attr('disabled', false);
        $('#deposito').attr('disabled', false);
    } else {
    	$('#tipoReembolso').attr('disabled', true);
    	$('#centrocusto').attr('disabled', true);
    	$('#nrdocumento').attr('disabled', true);
    	$('#valorDespesa').attr('disabled', true);
    	$('#dinheiro').attr('disabled', true);
        $('#deposito').attr('disabled', true);
    }
    	
	$('#pnlcodigo').hide();
	$('#pnlcpf').hide();
    $('#pnlcnpj').hide();
    
		        	
	$('#cpf').mask('000.000.000-00', {reverse: true});
	$('#cnpj').mask('00.000.000/0000-00', {reverse: true});
	$('#valor').mask("#.##0,00", {reverse: true});
	$('#valorDespesa').mask("#.##0,00", {reverse: true});
	
	//controla a exibicao dos controles de acordo com a selecao do usuario.
	$('#tipoBusca').change(function(){
		var selecionado = $(this).val();
				        
        if(selecionado === "ID"){
        	//$('#codigo').attr('req', true);
        	$('#pnlcodigo').show();
        	$('#pnlcpf').hide();
        	$('#pnlcnpj').hide();
        }
        if(selecionado === "CPF"){
        	$('#pnlcodigo').hide();
        	$('#pnlcpf').show();
        	$('#pnlcnpj').hide();
        }
        if(selecionado === "CNPJ"){
        	$('#pnlcodigo').hide();
        	$('#pnlcpf').hide();
        	$('#pnlcnpj').show();
        }
	});
	
	//controla a exibicao dos controles de acordo com a selecao do usuario.
	$('#tipoBuscaPrest').change(function(){
		var selecionado = $(this).val();
		
		if(selecionado === "ID"){
			//$('#codigo').attr('req', true);
			$('#pnlcodigo').show();
			$('#pnlCentroCusto').show();
			$('#pnlcnpj').hide();
			$('#pnlcpf').hide();
			$('#pnlcnpj').hide();
		}
		if(selecionado === "CPF"){
			$('#pnlcodigo').hide();
			$('#pnlCentroCusto').hide();
			$('#pnlcpf').show();
			$('#pnlcnpj').hide();
		}
		if(selecionado === "CNPJ"){
			$('#pnlcodigo').hide();
			$('#pnlCentroCusto').hide();
			$('#pnlcpf').hide();
			$('#pnlcnpj').show();
		}
	});

	//busca o favorecido a ser utilizado na tela
	$('#btnBuscarFavorecido').click(function() {
		
		var search = {}
		var tipo = $('#tipoBusca').val();
		
		if(tipo === undefined)
			tipo = "ID";
		
		if(tipo === "ID"){			
			var parametro = $('#codigo').val();
			search = {
				'parametro' : parametro,
				'tipo' : tipo 
			}
		}
		
		if(tipo === "CPF"){
			var parametro = $('#cpf').val();
			search = {
				'parametro' : parametro,
				'tipo' : tipo 
			}
		}
		
		if(tipo === "CNPJ"){
			var parametro = $('#cnpj').val();
			search = {
				'parametro' : parametro,
				'tipo' : tipo 
			}
		}

		$.ajax({
			type : 'POST',
			contentType : 'application/json; charset=utf-8',
			dataType : 'json',
			url : 'buscarFavorecido',
			data : JSON.stringify(search),
			beforeSend: function() {
				$("#loader").show();				
			},
			success : function(response) {
				$("#loader").hide();
				$('#favorecido').val(response.vendorName);

				//adiantamento
				$('#tipoAdiantamento').attr('disabled', false);
				$('#valor').attr('disabled', false);
				$('#detalhamento').attr('disabled', false);
				$('#dinheiro').attr('disabled', false);
				$('#deposito').attr('disabled', false);
			    
			    //reembolso
			    $('#tipoReembolso').attr('disabled', false);
			    $('#centrocusto').attr('disabled', false);
			    $('#nrdocumento').attr('disabled', false);
			    $('#valorDespesa').attr('disabled', false);
			},
			error: function(){      
            	$("#loader").hide();
			}
		});
		
	});
	
	//TODO Ed
	$('#btnBuscarAdiantamento').click(function() {
		
	});
	
	//recupera os dados bancarios do favorecido
	$('#deposito').click(function() {
		
		var search = {}
		var tipo = $('#tipoBusca').val();
		
		if(tipo === undefined){
			tipo = "ID";
		}
		
		if(tipo === "ID"){			
			var parametro = $('#codigo').val();
			search = {
				'parametro' : parametro,
				'tipo' : tipo 
			}
		}
		
		if(tipo === "CPF"){
			var parametro = $('#cpf').val();
			search = {
				'parametro' : parametro,
				'tipo' : tipo 
			}
		}
		
		if(tipo === "CNPJ"){
			var parametro = $('#cnpj').val();
			search = {
				'parametro' : parametro,
				'tipo' : tipo 
			}
		}
		
		$.ajax({
            type: 'POST',
            contentType : 'application/json; charset=utf-8',
            url: 'buscarDadosBancarios',
            dataType: 'json',
            data : JSON.stringify(search),
            beforeSend: function() {
            	$("#loader").show();
            },
            success: function(data){
            	$("#loader").hide();
            	if($.isEmptyObject(data)){
            		showAlert("Conta bancária não cadastrada para este fornecedor. Por Favor cadastrar esta informação na Oracle");
	            }else{	            	 
	            	$.each(data, function(index, value) {
	            		$('#dadosBancarios').append($('<option>').text(value).val(index));
	                });	            	 
	            	
	            	$('#dadosBancarios').attr('disabled', false);
	            }
            },
            error: function(){      
            	$("#loader").hide();
			}
        });
	});
	
	$('#dinheiro').click(function() {
		$('#dadosBancarios').attr('disabled', true);
	});
	
	
	$('#btnNotaAdiantamento').click(function() {
		document.location = $('#context').val() + 'adiantamento-form';
	});
	
	$('#btnPrestacaoConta').click(function() {
		document.location = $('#context').val() + 'prestacao-form';
	});

	$('#btnReembolso').click(function() {
		document.location = $('#context').val() + 'reembolso-form';
	});
	$('#btnImpAdiantamento').click(function() {
		document.location = $('#context').val() + 'reembolso-form';
	});

});

function showSuccess(text) {
	$('#modalAlert .modal-body').text(text);
	$('#modalAlert .btn').removeClass('btn-danger');
	$('#modalAlert .btn').addClass('btn-success');
	$('#modalAlert .btn').click(function() {
		$('#modalAlert .btn').attr('data-dismiss', 'modal');
	});
	$('#btnModalAlert').click();
}

function showAlert(text) {
	$('#modalAlert .modal-body').text(text);
	//$('#modalAlert .btn').removeClass('btn-danger');
	//$('#modalAlert .btn').addClass('btn-success');
	$('#modalAlert .btn').click(function() {
		$('#modalAlert .btn').attr('data-dismiss', 'modal');
	});
	$('#btnModalAlert').click();
}