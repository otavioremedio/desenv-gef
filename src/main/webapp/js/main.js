(function($) {
	$(window).load(function() {		
			$("#loader").addClass("carregado");
			
			$("select").change(function() {
				$(this).addClass("selecionado");
			});
			
			$("footer nav.menu-footer ul.menu > li > ul > li .sub-menu").slideUp();
			
			$("footer nav.menu-footer ul.menu > li > ul > li .clique").click(function(){
				$(this).parent().children(".sub-menu").slideToggle();
				$(this).parent().toggleClass("ativo");
			});
			
			/*$(".sub-menu .sub-menu").each(function() {
				$(this).css({
					"max-height" : $(this).height(),
					"height" : 0
				});
			});*/

    /* Script do Menu Mobile que estava na página. Retirado para ter somenten um aquivo JS */ 
    jQuery(document).ready(function($) {  
   
     var ResponsiveMenu = {
       trigger: '#responsive-menu-button',
       animationSpeed: 500,
       breakpoint: 800,
       pushButton: 'off',
       animationType: 'push',
       animationSide: 'left',
       pageWrapper: '',
       isOpen: false,
       triggerTypes: 'click',
       activeClass: 'is-active',
       container: '#responsive-menu-container',
       openClass: 'responsive-menu-open',
       accordion: 'off',
       activeArrow: '<i class="fa fa-caret-up" aria-hidden="true"></i>',
       inactiveArrow: '<i class="fa fa-caret-down" aria-hidden="true"></i>',
       wrapper: '#responsive-menu-wrapper',
       closeOnBodyClick: 'off',
       closeOnLinkClick: 'off',
       itemTriggerSubMenu: 'off',
       linkElement: '.responsive-menu-item-link',
       openMenu: function() {
         $(this.trigger).addClass(this.activeClass);
         $('html').addClass(this.openClass);
         $('.responsive-menu-button-icon-active').hide();
         $('.responsive-menu-button-icon-inactive').show();
         this.setWrapperTranslate();
         this.isOpen = true;
       },
       closeMenu: function() {
         $(this.trigger).removeClass(this.activeClass);
         $('html').removeClass(this.openClass);
         $('.responsive-menu-button-icon-inactive').hide();
         $('.responsive-menu-button-icon-active').show();
         this.clearWrapperTranslate();
         this.isOpen = false;
       },
       triggerMenu: function() {
         this.isOpen ? this.closeMenu() : this.openMenu();
       },
       triggerSubArrow: function(subarrow) {
         var sub_menu = $(subarrow).parent().next('.responsive-menu-submenu');
         var self = this;
         if(this.accordion == 'on') {
           /* Get Top Most Parent and the siblings */
           var top_siblings = sub_menu.parents('.responsive-menu-item-has-children').last().siblings('.responsive-menu-item-has-children');
           var first_siblings = sub_menu.parents('.responsive-menu-item-has-children').first().siblings('.responsive-menu-item-has-children');
           /* Close up just the top level parents to key the rest as it was */
           top_siblings.children('.responsive-menu-submenu').slideUp(200, 'linear').removeClass('responsive-menu-submenu-open');
           /* Set each parent arrow to inactive */
           top_siblings.each(function() {
             $(this).find('.responsive-menu-subarrow').first().html(self.inactiveArrow);
             $(this).find('.responsive-menu-subarrow').first().removeClass('responsive-menu-subarrow-active');
           });
           /* Now Repeat for the current item siblings */
           first_siblings.children('.responsive-menu-submenu').slideUp(200, 'linear').removeClass('responsive-menu-submenu-open');
           first_siblings.each(function() {
             $(this).find('.responsive-menu-subarrow').first().html(self.inactiveArrow);
             $(this).find('.responsive-menu-subarrow').first().removeClass('responsive-menu-subarrow-active');
           });
         }
         if(sub_menu.hasClass('responsive-menu-submenu-open')) {
           sub_menu.slideUp(200, 'linear').removeClass('responsive-menu-submenu-open');
           $(subarrow).html(this.inactiveArrow);
           $(subarrow).removeClass('responsive-menu-subarrow-active');
         } else {
           sub_menu.slideDown(200, 'linear').addClass('responsive-menu-submenu-open');
           $(subarrow).html(this.activeArrow);
           $(subarrow).addClass('responsive-menu-subarrow-active');
         }
       },
       menuHeight: function() {
         return $(this.container).height();
       },
       menuWidth: function() {
         return $(this.container).width();
       },
       wrapperHeight: function() {
         return $(this.wrapper).height();
       },
       setWrapperTranslate: function() {
         switch(this.animationSide) {
           case 'left':
             translate = 'translateX(' + this.menuWidth() + 'px)'; break;
           case 'right':
             translate = 'translateX(-' + this.menuWidth() + 'px)'; break;
           case 'top':
             translate = 'translateY(' + this.wrapperHeight() + 'px)'; break;
           case 'bottom':
             translate = 'translateY(-' + this.menuHeight() + 'px)'; break;
           }
           if(this.animationType == 'push') {
             $(this.pageWrapper).css({'transform':translate});
             $('html, body').css('overflow-x', 'hidden');
           }
           if(this.pushButton == 'on') {
             $('#responsive-menu-button').css({'transform':translate});
           }
       },
       clearWrapperTranslate: function() {
         var self = this;
         if(this.animationType == 'push') {
           $(this.pageWrapper).css({'transform':''});
           setTimeout(function() {
             $('html, body').css('overflow-x', '');
           }, self.animationSpeed);
         }
         if(this.pushButton == 'on') {
           $('#responsive-menu-button').css({'transform':''});
         }
       },
       init: function() {
         var self = this;
         $(this.trigger).on(this.triggerTypes, function(e){
           e.stopPropagation();
           self.triggerMenu();
         }); 
         $('.responsive-menu-subarrow').on('click', function(e) { 
           e.preventDefault();
           e.stopPropagation();
           self.triggerSubArrow(this);
         });
         $(window).resize(function() {
           if($(window).width() > self.breakpoint) {
             if(self.isOpen){
               self.closeMenu();
             }
           } else {
             if($('.responsive-menu-open').length>0){
               self.setWrapperTranslate();
             }
           }
         });
         if(this.closeOnLinkClick == 'on') {
           $(this.linkElement).on('click', function(e) {
             e.preventDefault();
             /* Fix for when close menu on parent clicks is on */
             if(self.itemTriggerSubMenu == 'on' && $(this).is('.responsive-menu-item-has-children > ' + self.linkElement)) {
               return;
             }
             old_href = $(this).attr('href');
             old_target = typeof $(this).attr('target') == 'undefined' ? '_self' : $(this).attr('target');
             if(self.isOpen) {
               if($(e.target).closest('.responsive-menu-subarrow').length) {
                 return;
               }
               self.closeMenu();
               setTimeout(function() {
                 window.open(old_href, old_target);
               }, self.animationSpeed);
             }
           });
         }
         if(this.closeOnBodyClick == 'on') {
           $(document).on('click', 'body', function(e) {
             if(self.isOpen) {
               if($(e.target).closest('#responsive-menu-container').length || $(e.target).closest('#responsive-menu-button').length) {
                 return;
               }
             }
             self.closeMenu();
           });
         }
         if(this.itemTriggerSubMenu == 'on') {
           $('.responsive-menu-item-has-children > ' + this.linkElement).on('click', function(e) {
             e.preventDefault();
             self.triggerSubArrow($(this).children('.responsive-menu-subarrow').first());
           });
         }
       }
     };
     ResponsiveMenu.init();
   });
 /* Fim Script do Menu Mobile que estava na página. Retirado para ter somenten um aquivo JS */  







	});
	
var ppp = 3; // Post per page
var pageNumber = 1;

function load_posts(){
    pageNumber++;
    var str = '&pageNumber=' + pageNumber + '&ppp=' + ppp + '&offset=' + ppp*(pageNumber-1)+1 + '&action=more_post_ajax';
    $.ajax({
        type: "POST",
        dataType: "html",
        url: ajax_posts.ajaxurl,
        data: str,
        success: function(data){
            var $data = $(data);
            if($data.length){
                $(".infinito").append($data);
				$(".item").removeClass("animacao");
                $(".carregar-noticias")
				.attr("disabled",false)
				.html("Ver mais <strong>+</strong>");
            } else {
                $(".carregar-noticias")
				.attr("disabled",true)
				.html("Todos os itens carregados")
				.removeClass("carregar");
            }
        },
        error : function(jqXHR, textStatus, errorThrown) {
            $loader.html(jqXHR + " :: " + textStatus + " :: " + errorThrown);
        }

    });
    return false;
}

$(".carregar-noticias").on("click",function(){ // When btn is pressed.
    $(".carregar-noticias")
	.attr("disabled",true) // Disable the button, temp.
	.html("Carregando...");
    load_posts();
});

function load_cat(){
    pageNumber++;
    var str = '&pageNumber=' + pageNumber + '&ppp=' + ppp + '&offset=' + ppp*(pageNumber-1)+3 + '&action=more_cat_ajax';
    $.ajax({
        type: "POST",
        dataType: "html",
        url: ajax_posts.ajaxurl,
        data: str,
        success: function(data){
            var $data = $(data);
            if($data.length){
                $(".infinito").append($data);
				$(".item").removeClass("animacao");
                $(".carregar-categoria")
				.attr("disabled",false)
				.html("Ver mais <strong>+</strong>");
            } else {
                $(".carregar-categoria")
				.attr("disabled",true)
				.html("Todos os itens carregados")
				.removeClass("carregar");
            }
        },
        error : function(jqXHR, textStatus, errorThrown) {
            $loader.html(jqXHR + " :: " + textStatus + " :: " + errorThrown);
        }

    });
    return false;
}

$(".carregar-categoria").on("click",function(){ // When btn is pressed.
    $(".carregar-categoria")
	.attr("disabled",true) // Disable the button, temp.
	.html("Carregando...");
    load_cat();
});


function load_eventos(){
    pageNumber++;
    var str = '&pageNumber=' + pageNumber + '&ppp=' + ppp + '&offset=' + ppp*(pageNumber-1)+2 + '&action=more_eventos_ajax';
    $.ajax({
        type: "POST",
        dataType: "html",
        url: ajax_posts.ajaxurl,
        data: str,
        success: function(data){
            var $data = $(data);
            if($data.length){
                $(".infinito").append($data);
				$(".item").removeClass("animacao");
                $(".carregar-eventos")
				.attr("disabled",false)
				.html("Ver mais <strong>+</strong>");
            } else {
                $(".carregar-eventos")
				.attr("disabled",true)
				.html("Todos os itens carregados")
				.removeClass("carregar");
            }
        },
        error : function(jqXHR, textStatus, errorThrown) {
            $loader.html(jqXHR + " :: " + textStatus + " :: " + errorThrown);
        }

    });
    return false;
}

$('.carregar-eventos').on("click",function(){ // When btn is pressed.
    $(".carregar-eventos")
	.attr("disabled",true) // Disable the button, temp.
	.html("Carregando...");
    load_eventos();
});


function load_tema(){
    pageNumber++;
    var str = '&pageNumber=' + pageNumber + '&ppp=' + ppp + '&offset=' + ppp*(pageNumber-1)+2 + '&action=more_tema_ajax';
    $.ajax({
        type: "POST",
        dataType: "html",
        url: ajax_posts.ajaxurl,
        data: str,
        success: function(data){
            var $data = $(data);
            if($data.length){
                $(".infinito").append($data);
				$(".item").removeClass("animacao");    
                $(".carregar-tema")
				.attr("disabled",false)
				.html("Ver mais <strong>+</strong>");
            } else {
                $(".carregar-tema")
				.attr("disabled",true)
				.html("Todos os itens carregados")
				.removeClass("carregar");
            }
        },
        error : function(jqXHR, textStatus, errorThrown) {
            $loader.html(jqXHR + " :: " + textStatus + " :: " + errorThrown);
        }

    });
    return false;
}

$('.carregar-tema').on("click",function(){ // When btn is pressed.
    $(".carregar-tema")
	.attr("disabled",true) // Disable the button, temp.
	.html("Carregando...");
    load_tema();
});

$(document).ready(function(){
    $(".dropdown").hover(            
        function() {
            $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true,true);
            $(this).toggleClass('open');        
        },
        function() {
            $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true,true);    
            $(this).removeClass('open');          
        }
    ); 

    $('.menunew_portal .navbar-nav a.dropdown-toggle').click(function(){
      return false;  
    })  

    // $('.menunew_portal .dropdown-menu>li>a').click(function(){
    //   $('.menunew_portal .navbar-nav>li').removeClass('open');    
     
    // })  

   
});      
	
}(jQuery));







